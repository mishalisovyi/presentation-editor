import { Component, OnInit } from '@angular/core';
import { FabricjsService } from '../../../services/fabricjs/fabricjs.service';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.scss']
})
export class OtherComponent implements OnInit {

  constructor(
    private fabricjsService: FabricjsService
  ) { }

  ngOnInit() {
    this.image = this.fabricjsService.editedSlide.background.image;
    this.getSlideBackgroundProperties();
  }

  image: string = '';
  backgroundType: string = 'color';
  slideBackgroundProperties = {
    color: '',
    image: ''
  }

  saveSlideProperties = () => {
    this.fabricjsService.getSlideState();
    this.fabricjsService.toggleObjectsSelectability('all', true);
    this.fabricjsService.typeOfEditing.other = false;
    this.fabricjsService.typeOfEditing.list = true;
  }

  setBackgroundImage = () => {
    this.fabricjsService.setBackgroundImage(this.image, this.fabricjsService.canvas);
  }

  readImage($event: any): void {
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.image = myReader.result;
    }
    if (file) {
      myReader.readAsDataURL(file);
    }
  }

  getSlideBackgroundProperties = () => {
    this.slideBackgroundProperties.color = this.fabricjsService.editedSlide.background.color;
    this.slideBackgroundProperties.image = this.fabricjsService.editedSlide.background.image;
  }

  clearBackground = () => {
    this.fabricjsService.canvas.backgroundImage = false;
    this.slideBackgroundProperties.color = '';
    this.image = '';
    this.fabricjsService.canvas.backgroundColor = false;
    this.slideBackgroundProperties.image = '';
    this.fabricjsService.canvas.requestRenderAll();
  }
}