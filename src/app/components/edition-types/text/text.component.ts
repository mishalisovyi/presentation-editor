import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { FabricjsService } from '../../../services/fabricjs/fabricjs.service';
import { TextService } from '../../../services/fabric-objects/text/text.service';
import { Constants } from '../../../classes/constants'

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {

  addTextForm: FormGroup;
  newText: FormControl;
  newTitle: FormControl;
  fontFamilies: string[];
  fontSizes: number[];
  lineHeights: number[];
  idCounter: number;

  constructor(
    private fabricjsService: FabricjsService,
    private textService: TextService
  ) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    this.fontFamilies = Constants.fontFamilies;
    this.fontSizes = Constants.fontSizes;
    this.lineHeights = Constants.lineHeights;
    this.getIdCounter();
    if (this.fabricjsService.componentRole === 'add') {
      this.textService.resetActiveText();
    }
  }

  createFormControls = (): void => {
    this.newText = new FormControl('', Validators.required);
    this.newTitle = new FormControl('', Validators.required);
  }

  createForm = (): void => {
    this.addTextForm = new FormGroup({
      newText: this.newText,
      newTitle: this.newTitle
    });
  }

  addText = () => {
    this.textService.addTextTry = true;
    if (this.textService.activeText.body && this.textService.activeText.title) {
      this.textService.addText(this.fabricjsService.canvas, this.idCounter);
      ++this.idCounter;
    }
  }

  changeTextTitle = () => {
    if (this.fabricjsService.canvas.getActiveObject()) {
      this.fabricjsService.canvas.getActiveObject().title = this.textService.activeText.title;
    } else {
      if (this.fabricjsService.componentRole === 'edit') {
        this.textService.getEditedFabricObject(this.fabricjsService.canvas).title = this.textService.activeText.title;
      }
    }
  }

  getIdCounter = () => {
    if (this.fabricjsService.editedSlide.texts.length > 0) {
      this.idCounter = this.fabricjsService.editedSlide.texts[0].id;
      for (let i = 1; i < this.fabricjsService.editedSlide.texts.length; ++i) {
        if (this.idCounter < this.fabricjsService.editedSlide.texts[i].id) {
          this.idCounter = this.fabricjsService.editedSlide.texts[i].id;
        }
      }
      ++this.idCounter;
    } else {
      this.idCounter = 0;
    }
  }

  save = () => {
    for (let i = 0; i < this.fabricjsService.canvas._objects.length; ++i) {
      this.fabricjsService.canvas._objects[i].off('deselected');
    }
    this.fabricjsService.getSlideState();
    this.fabricjsService.toggleObjectsSelectability('all', true);
    this.fabricjsService.typeOfEditing.text = false;
    this.fabricjsService.typeOfEditing.list = true;
    this.fabricjsService.canvas.off('selection:created');
    this.fabricjsService.canvas.off('selection:updated');
    this.fabricjsService.canvas.off('selection:cleared');
  }
}