import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { FabricjsService } from '../../../services/fabricjs/fabricjs.service';
import { AnimationService } from '../../../services/fabric-objects/animation/animation.service';
import { Constants } from '../../../classes/constants'

@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.scss']
})
export class AnimationComponent implements OnInit {

  animationForm: FormGroup;

  property: FormControl;
  value: FormControl;
  duration: FormControl;
  easing: FormControl;
  startAfter: FormControl;
  positionX: FormControl;
  positionY: FormControl;
  title: FormControl;

  animationPropertiesArray: string[];
  easingsArray: any[];
  propertyStep: number;
  idCounter: number;
  submitMode: string;
  potentialPredecessors = []
  animationList = true;
  disableAnimationSubmit = false;
  animationProperties = false;
  submitAnimationTry = false;
  propertyMinValue = 0;
  propertyMaxValue = 360;
  showIncorrectValueMessage = false;
  showIncorrectDurationMessage = false;
  warningValueMessage = '';
  warningDurationMessage = '';

  constructor(
    public fabricjsService: FabricjsService,
    private animationService: AnimationService
  ) { }

  ngOnInit() {
    this.animationPropertiesArray = Constants.animationProperties;
    this.easingsArray = Constants.easings;
    this.createFormControls();
    this.createForm();
    this.getIdCounter();
    this.fabricjsService.canvas.on('selection:cleared', this.keepObjectSelected);
  }

  createFormControls = (): void => {
    this.property = new FormControl('', Validators.required);
    this.value = new FormControl('', Validators.required);
    this.duration = new FormControl('', Validators.required);
    this.easing = new FormControl('', Validators.required);
    this.startAfter = new FormControl('', Validators.required);
    this.positionX = new FormControl('', Validators.required);
    this.positionY = new FormControl('', Validators.required);
    this.title = new FormControl('', Validators.required);
  }

  createForm = (): void => {
    this.animationForm = new FormGroup({
      property: this.property,
      positionX: this.positionX,
      positionY: this.positionY,
      value: this.value,
      duration: this.duration,
      easing: this.easing,
      startAfter: this.startAfter,
      title: this.title
    });
  }

  changeEasingDisplay = () => {
    for (let i = 0; i < this.easingsArray.length; ++i) {
      if (this.easingsArray[i].value === this.animationService.activeAnimation.easing) {
        this.animationService.activeAnimation.displayEasing = this.easingsArray[i].title;
        break;
      }
    }
  }

  changeDisplayStartAfter = () => {
    for (let i = 0; i < this.potentialPredecessors.length; ++i) {
      if (this.potentialPredecessors[i].id === this.animationService.activeAnimation.startAfter) {
        this.animationService.activeAnimation.displayStartAfter = this.potentialPredecessors[i].title;
        break;
      }
    }
  }

  getIdCounter = () => {
    if (this.fabricjsService.editedSlide.animations.length > 0) {
      this.idCounter = this.fabricjsService.editedSlide.animations[0].id;
      for (let i = 1; i < this.fabricjsService.editedSlide.animations.length; ++i) {
        if (this.idCounter < this.fabricjsService.editedSlide.animations[i].id) {
          this.idCounter = this.fabricjsService.editedSlide.animations[i].id;
        }
      }
      ++this.idCounter;
    } else {
      this.idCounter = 0;
    }
  }

  submitAnimationForm = (): void => {
    const generalPropertiesValid = this.title.valid && this.property.valid && this.duration.valid && this.easing.valid && this.startAfter.valid;
    let specificPropertiesValid: boolean;
    const correctInputValues = !this.showIncorrectDurationMessage && !this.showIncorrectValueMessage;
    if (this.animationService.activeAnimation.property === 'position') {
      specificPropertiesValid = this.positionX.valid && this.positionY.valid;
    } else {
      specificPropertiesValid = this.value.valid;
    }
    if (generalPropertiesValid && specificPropertiesValid && correctInputValues) {
      if (this.submitMode === 'preview') {
        this.animationService.playSingleAnimation(this.fabricjsService, true, null);
      } else {
        this.saveAnimation();
      }
    }
  }

  deleteAnimation = (id: number): void => {
    console.log(this.fabricjsService.editedSlide.animations);
    let deletedAnimationsId: number;
    for (let i = 0; i < this.fabricjsService.editedSlide.animations.length; ++i) {
      if (this.fabricjsService.editedSlide.animations[i].id === id) {
        deletedAnimationsId = this.fabricjsService.editedSlide.animations[i].id;
        this.fabricjsService.editedSlide.animations.splice(i, 1);
        break;
      }
    }
    this.animationService.correctStartAfter(this.fabricjsService, deletedAnimationsId);
    this.animationService.correctNextAnimations(this.fabricjsService, deletedAnimationsId);
    this.fabricjsService.getActiveObjectAnimations();
    console.log(this.fabricjsService.editedSlide.animations);
  }

  toggleViews = () => {
    this.animationList = !this.animationList;
    this.animationProperties = !this.animationProperties;
  }

  getWarningMessage = (min: number, max: number): string => {
    return `Value must be from ${min} to ${max}`;
  }

  switchProperty = (onLoad: boolean) => {
    this.showIncorrectValueMessage = false;
    switch (this.animationService.activeAnimation.property) {
      case 'position':
        this.fabricjsService.pointer.isLocked = false;
        break;
      case 'angle':
        this.fabricjsService.deletePointer();
        this.propertyStep = 1;
        this.propertyMinValue = 0;
        this.propertyMaxValue = 360;
        if (!onLoad) {
          this.animationService.activeAnimation.value.common = 0;
        }
        break;
      case 'opacity':
        this.fabricjsService.deletePointer();
        this.propertyStep = 0.01;
        this.propertyMinValue = 0;
        this.propertyMaxValue = 1;
        if (!onLoad) {
          this.animationService.activeAnimation.value.common = 1;
        }
        break;
      default:
        this.fabricjsService.deletePointer();
        this.propertyStep = 0.01;
        if (this.animationService.activeAnimation.hostType === 'i-text') {
          this.propertyMinValue = 1;
        } else {
          this.propertyMinValue = 0.2;
        }
        this.propertyMaxValue = 9;
        if (!onLoad) {
          this.animationService.activeAnimation.value.common = 1;
        }
        break;
    }
  }

  getSubmitMode = (mode: string): void => {
    this.submitAnimationTry = true;
    this.submitMode = mode;
  }

  changePropertyValue = (value: number): void => {
    if (value.toString().length > 4) {
      value = Math.floor(value * 100) / 100;
    };
    this.animationService.activeAnimation.value.common = value;
  }

  checkValueCorrect = () => {
    if (this.animationService.activeAnimation.value.common === null) {
      this.showIncorrectValueMessage = false;
    } else if (this.animationService.activeAnimation.value.common < this.propertyMinValue || this.animationService.activeAnimation.value.common > this.propertyMaxValue) {
      this.showIncorrectValueMessage = true;
      this.warningValueMessage = this.getWarningMessage(this.propertyMinValue, this.propertyMaxValue);
    } else {
      this.showIncorrectValueMessage = false;
    }
  }

  checkDurationCorrect = () => {
    if (this.animationService.activeAnimation.duration === null) {
      this.showIncorrectDurationMessage = false;
    } else if (this.animationService.activeAnimation.duration < 100 || this.animationService.activeAnimation.duration > 10000) {
      this.showIncorrectDurationMessage = true;
      this.warningDurationMessage = 'Duration must be from 100 to 10000';
    } else {
      this.showIncorrectDurationMessage = false;
    }
  }

  saveAnimation = (): void => {
    this.fabricjsService.canvas.forEachObject((obj) => {
      obj.selectable = true;
    });
    if (this.animationService.activeAnimation.property === 'position') {
      this.animationService.activeAnimation.value.x = this.fabricjsService.pointer.x;
      this.animationService.activeAnimation.value.y = this.fabricjsService.pointer.y;
    }
    if (this.animationService.activeAnimation.startAfter >= 0) {
      for (let i = 0; i < this.fabricjsService.editedSlide.animations.length; ++i) {
        if (this.fabricjsService.editedSlide.animations[i].id === this.animationService.activeAnimation.startAfter) {
          if (this.fabricjsService.editedSlide.animations[i].nextAnimations.indexOf(this.animationService.activeAnimation.id) < 0) {
            this.fabricjsService.editedSlide.animations[i].nextAnimations.push(this.animationService.activeAnimation.id);
          }
          break;
        }
      }
    }
    if (this.animationService.isChangedAnimation) {
      for (let i = 0; i < this.fabricjsService.editedSlide.animations.length; ++i) {
        if (this.fabricjsService.editedSlide.animations[i].id === this.animationService.activeAnimation.id) {
          this.fabricjsService.editedSlide.animations[i] = JSON.parse(JSON.stringify(this.animationService.activeAnimation));
          break;
        }
      }
      for (let i = 0; i < this.fabricjsService.editedSlide.animations.length; ++i) {
        if (this.fabricjsService.editedSlide.animations[i].startAfter === this.animationService.activeAnimation.id) {
          this.fabricjsService.editedSlide.animations[i].displayStartAfter = this.animationService.activeAnimation.title;
        }
      }
      this.animationService.isChangedAnimation = false;
    }
    if (this.animationService.isAddingAnimation) {
      this.fabricjsService.editedSlide.animations.push(JSON.parse(JSON.stringify(this.animationService.activeAnimation)));
      this.animationService.isAddingAnimation = false;
    }
    this.fabricjsService.pointer.isLocked = true;
    this.fabricjsService.deletePointer();
    this.fabricjsService.getActiveObjectAnimations();
    this.potentialPredecessors = JSON.parse(JSON.stringify(this.fabricjsService.editedSlide.animations));
    this.toggleViews();
  }

  saveAnimations = () => {
    this.fabricjsService.typeOfEditing.animation = false;
    this.fabricjsService.typeOfEditing.list = true;
  }

  changePredecessor = () => {
    this.disableAnimationSubmit = false;
    let countOfStartAfterLoadingSlide = 0;
    let initialAnimationId: number;
    for (let i = 0; i < this.fabricjsService.editedSlide.animations.length; ++i) {
      if (this.fabricjsService.editedSlide.animations[i].startAfter === -1) {
        ++countOfStartAfterLoadingSlide;
        initialAnimationId = this.fabricjsService.editedSlide.animations[i].id;
      }
      for (let j = 0; j < this.fabricjsService.editedSlide.animations[i].nextAnimations.length; ++j) {
        if (this.fabricjsService.editedSlide.animations[i].nextAnimations[j] === this.animationService.activeAnimation.id) {
          this.fabricjsService.editedSlide.animations[i].nextAnimations.splice(j, 1);
          break;
        }
      }
    }
    if (this.animationService.activeAnimation.startAfter !== -1 && countOfStartAfterLoadingSlide < 2 && initialAnimationId === this.animationService.activeAnimation.id) {
      alert('There are no more animations which start after slide loading\nAt least one animation must start after slide loading!');
      this.disableAnimationSubmit = true;
    }
    this.changeDisplayStartAfter();
  }

  addAnimation = (): void => {
    const activeObject = this.fabricjsService.canvas.getActiveObject();
    this.fabricjsService.canvas.forEachObject((obj) => {
      if ((obj.type !== activeObject.type) || (obj.type === activeObject.type && obj.id !== activeObject.id)) {
        obj.selectable = false;
      }
      if (obj.type === 'i-text') {
        obj.editable = false;
      }
    });
    this.animationService.isAddingAnimation = true;
    let newAnimation = {
      hostId: this.fabricjsService.canvas.getActiveObject().id,
      hostType: this.fabricjsService.canvas.getActiveObject().type,
      id: this.idCounter,
      title: `Animation ${this.idCounter + 1}`,
      property: 'angle',
      value: {
        common: 0,
        x: null,
        y: null
      },
      duration: 1000,
      easing: 'easeInQuad',
      displayEasing: 'Slow increase',
      startAfter: -1,
      displayStartAfter: 'loading slide',
      nextAnimations: []
    }
    this.animationService.activeAnimation = JSON.parse(JSON.stringify(newAnimation));
    this.getPotentialPredecessors();
    this.toggleViews();
    ++this.idCounter;
  }

  cancel = () => {
    this.disableAnimationSubmit = false;
    this.potentialPredecessors = JSON.parse(JSON.stringify(this.fabricjsService.editedSlide.animations));
    if (this.animationService.isAddingAnimation) {
      --this.idCounter;
    }
    this.animationService.isChangedAnimation = false;
    this.animationService.isAddingAnimation = false;
    this.fabricjsService.canvas.forEachObject((obj) => {
      obj.selectable = true;
    });
    this.fabricjsService.deletePointer();
    this.toggleViews();
  }

  keepObjectSelected = (e: any) => {
    if (this.animationService.isChangedAnimation || this.animationService.isAddingAnimation) {
      const object = e.deselected[0];
      this.fabricjsService.canvas.setActiveObject(object);
    }
  }

  editAnimation = (id: number, hostType: string): void => {
    this.fabricjsService.canvas.forEachObject((obj) => {
      if ((obj.type !== hostType) || (obj.type === hostType && obj.id !== id)) {
        obj.selectable = false;
      }
      if (obj.type === 'i-text') {
        obj.editable = false;
      }
    });
    this.animationService.isChangedAnimation = true;
    for (let i = 0; i < this.animationService.activeObjectAnimations.length; ++i) {
      if (this.animationService.activeObjectAnimations[i].id === id) {
        this.animationService.activeAnimation = JSON.parse(JSON.stringify(this.animationService.activeObjectAnimations[i]));
        break;
      }
    }
    if (this.animationService.activeAnimation.property === 'position') {
      this.fabricjsService.pointer.x = this.animationService.activeAnimation.value.x;
      this.fabricjsService.pointer.y = this.animationService.activeAnimation.value.y;
    }
    this.getPotentialPredecessors();
    this.switchProperty(true);
    this.toggleViews();
  }

  getPotentialPredecessors = () => {
    let flag = false;
    this.potentialPredecessors.length = 0;
    for (let i = 0; i < this.fabricjsService.editedSlide.animations.length; ++i) {
      for (let j = 0; j < this.animationService.activeAnimation.nextAnimations.length; ++j) {
        if (this.animationService.activeAnimation.nextAnimations[j] === this.fabricjsService.editedSlide.animations[i].id) {
          flag = true;
          break;
        }
      }
      if (flag) {
        flag = false;
        continue;
      }
      if (this.fabricjsService.editedSlide.animations[i].id !== this.animationService.activeAnimation.id) {
        this.potentialPredecessors.push(JSON.parse(JSON.stringify(this.fabricjsService.editedSlide.animations[i])));
      }
    }
    this.potentialPredecessors.sort((a, b) => {
      if (a.title > b.title) {
        return 1;
      }
      if (a.title < b.title) {
        return -1;
      }
      return 0;
    });
  }
}