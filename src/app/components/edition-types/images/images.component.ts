import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { FabricjsService } from '../../../services/fabricjs/fabricjs.service';
import { ImageService } from '../../../services/fabric-objects/image/image.service';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {

  titleForm: FormGroup;
  titleText: FormControl;
  changedImageBody: boolean = false;
  isAddedImage: boolean = false;
  imageSize = {
    width: null,
    height: null
  }
  idCounter: number;

  constructor(
    private fabricjsService: FabricjsService,
    private imageService: ImageService
  ) { }

  ngOnInit() {
    this.createFormControl();
    this.createForm();
    this.setIsActiveImage();
    this.getIdCounter();
    if (this.fabricjsService.componentRole === 'add') {
      this.imageService.resetActiveImage();
    }
  }

  createFormControl = (): void => {
    this.titleText = new FormControl('', Validators.required);
  }

  createForm = (): void => {
    this.titleForm = new FormGroup({
      titleText: this.titleText
    });
  }

  setIsActiveImage = () => {
    this.imageService.isActiveImage.length = 0;
    for (let i = 0; i < this.fabricjsService.editedSlide.images.length; ++i) {
      const image = {
        isActive: false,
        id: this.fabricjsService.editedSlide.images[i].id
      }
      this.imageService.isActiveImage.push(image);
    }
  }

  getIdCounter = () => {
    if (this.fabricjsService.editedSlide.images.length > 0) {
      this.idCounter = this.fabricjsService.editedSlide.images[0].id;
      for (let i = 1; i < this.fabricjsService.editedSlide.images.length; ++i) {
        if (this.idCounter < this.fabricjsService.editedSlide.images[i].id) {
          this.idCounter = this.fabricjsService.editedSlide.images[i].id;
        }
      }
      ++this.idCounter;
    } else {
      this.idCounter = 0;
    }
  }

  changeImageTitle = () => {
    if (this.fabricjsService.canvas.getActiveObject()) {
      this.fabricjsService.canvas.getActiveObject().title = this.imageService.activeImage.title;
    } else {
      if (this.fabricjsService.componentRole === 'edit') {
        this.imageService.getEditedFabricObject(this.fabricjsService.canvas).title = this.imageService.activeImage.title;
      }
    }
  }

  readImage($event: any): void {
    let nameWithExtension = $event.target.files[0].name;
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.imageService.activeImage.body = myReader.result;
      if (!this.fabricjsService.canvas.getActiveObject() && !this.imageService.activeImage.title) {
        this.imageService.activeImage.title = this.getNewImageTitle();
      }
      this.changedImageBody = true;
      let newImage = new Image();
      newImage.src = this.imageService.activeImage.body;
      newImage.onload = () => {
        this.imageSize.width = newImage.width;
        this.imageSize.height = newImage.height;
      }
    }
    myReader.readAsDataURL(file);
  }

  getNewImageTitle = (): string => {
    const titleName = 'Image';
    let countOfObjects = 0;
    this.fabricjsService.canvas.forEachObject((object) => {
      if (object.type === 'image') {
        ++countOfObjects;
      }
    });
    return `${titleName} ${countOfObjects + 1}`;
  }

  addImageToCanvas = () => {
    this.imageService.addImageTry = true;
    if (!this.imageService.activeImage.angle) {
      this.imageService.activeImage.angle = 0;
    }
    if (!this.imageService.activeImage.scaleX) {
      this.imageService.activeImage.scaleX = 1;
    }
    if (!this.imageService.activeImage.scaleY) {
      this.imageService.activeImage.scaleY = 1;
    }
    if (!this.imageService.activeImage.opacity) {
      this.imageService.activeImage.opacity = 1;
    }
    this.imageService.activeImage.left = this.imageSize.width * this.imageService.activeImage.scaleX / 2;
    this.imageService.activeImage.top = this.imageSize.height * this.imageService.activeImage.scaleY / 2;
    this.imageService.activeImage.id = this.idCounter;
    this.changedImageBody = false;
    this.isAddedImage = true;
    if (this.imageService.activeImage.body && this.imageService.activeImage.title) {
      this.imageService.addExistingImageToCanvas(this.imageService.activeImage, this.fabricjsService.canvas, false, this.fabricjsService.componentRole);
    }
    ++this.idCounter;
  }

  saveImage = () => {
    this.fabricjsService.canvas.discardActiveObject();
    if (this.fabricjsService.componentRole === 'edit') {
      for (let i = 0; i < this.fabricjsService.canvas._objects.length; ++i) {
        this.fabricjsService.canvas._objects[i].off('deselected');
        if (
          this.fabricjsService.canvas._objects[i].type === 'image' &&
          this.fabricjsService.canvas._objects[i].id === this.imageService.activeImage.id
        ) {
          this.fabricjsService.canvas._objects[i].title = this.imageService.activeImage.title;
        }
      }
      for (let i = 0; i < this.fabricjsService.editedSlide.images.length; ++i) {
        if (this.fabricjsService.editedSlide.images[i].id === this.imageService.activeImage.id) {
          this.fabricjsService.editedSlide.images[i].title = this.imageService.activeImage.title;
        }
      }
    }
    this.fabricjsService.getSlideState();
    this.fabricjsService.toggleObjectsSelectability('all', true);
    this.fabricjsService.typeOfEditing.images = false;
    this.fabricjsService.typeOfEditing.list = true;
    this.fabricjsService.canvas.off('selection:created');
    this.fabricjsService.canvas.off('selection:updated');
    this.fabricjsService.canvas.off('selection:cleared');
  }

  changeImage = () => {
    this.imageService.isChangingImage = true;
    this.fabricjsService.canvas.remove(this.fabricjsService.canvas.getActiveObject());
    this.imageService.addExistingImageToCanvas(this.imageService.activeImage, this.fabricjsService.canvas, false, this.fabricjsService.componentRole);
    this.changedImageBody = false;
  }
}