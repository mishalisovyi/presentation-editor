import { Component, OnInit, Input, ViewChild, ElementRef, HostListener } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { FabricjsService } from '../../../services/fabricjs/fabricjs.service';
import { FigureService } from '../../../services/fabric-objects/figure/figure.service';
import { Constants } from '../../../classes/constants'

@Component({
  selector: 'app-figures',
  templateUrl: './figures.component.html',
  styleUrls: ['./figures.component.scss']
})
export class FiguresComponent implements OnInit {

  @HostListener('window:resize')
  onResize() {
    this.setFigureSize();
  }

  @Input() bounds: string;
  @ViewChild('figureSpawner') figureSpawner: ElementRef;

  titleForm: FormGroup;
  title: FormControl;
  edge = {
    top: true,
    bottom: true,
    left: true,
    right: true
  };
  idCounter: number;
  figureSize: any;

  constructor(
    private fabricjsService: FabricjsService,
    private figureService: FigureService
  ) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    this.getIdCounter();
    this.setFigureSize();
    if (this.fabricjsService.componentRole === 'add') {
      this.figureService.resetActiveFigure();
    }
  }

  createFormControls = (): void => {
    this.title = new FormControl('', Validators.required);
  }

  createForm = (): void => {
    this.titleForm = new FormGroup({
      title: this.title
    });
  }

  checkEdge = (event: any): void => {
    this.edge = event;
  }

  setFigureSize = () => {
    if (this.fabricjsService.componentRole === 'add') {
      if (this.figureSpawner.nativeElement.offsetWidth > this.figureSpawner.nativeElement.offsetHeight) {
        this.figureSize = this.figureSpawner.nativeElement.offsetHeight;
      } else {
        this.figureSize = this.figureSpawner.nativeElement.offsetWidth;
      }
    }
  }

  changeFigureTitle = () => {
    if (this.fabricjsService.canvas.getActiveObject()) {
      this.fabricjsService.canvas.getActiveObject().title = this.figureService.activeFigure.title;
    } else {
      if (this.fabricjsService.componentRole === 'edit') {
        this.figureService.getEditedFabricObject(this.fabricjsService.canvas).title = this.figureService.activeFigure.title;
      }
    }
  }

  getIdCounter = () => {
    if (this.fabricjsService.editedSlide.figures.length > 0) {
      this.idCounter = this.fabricjsService.editedSlide.figures[0].id;
      for (let i = 1; i < this.fabricjsService.editedSlide.figures.length; ++i) {
        if (this.idCounter < this.fabricjsService.editedSlide.figures[i].id) {
          this.idCounter = this.fabricjsService.editedSlide.figures[i].id;
        }
      }
      ++this.idCounter;
    } else {
      this.idCounter = 0;
    }
  }

  saveFigure = () => {
    this.fabricjsService.canvas.discardActiveObject();
    for (let i = 0; i < this.fabricjsService.canvas._objects.length; ++i) {
      this.fabricjsService.canvas._objects[i].off('deselected');
    }
    this.fabricjsService.getSlideState();
    this.fabricjsService.toggleObjectsSelectability('all', true);
    this.fabricjsService.typeOfEditing.figures = false;
    this.fabricjsService.typeOfEditing.list = true;
    this.fabricjsService.canvas.off('selection:created');
    this.fabricjsService.canvas.off('selection:updated');
    this.fabricjsService.canvas.off('selection:cleared');
    this.fabricjsService.canvas.requestRenderAll();
  }

  onStop = (event: any, type: string): void => {
    if (this.edge.bottom && this.edge.left && this.edge.right && this.edge.top) {
      const pointer = this.fabricjsService.canvas.getPointer(event.e);
      this.fabricjsService.canvas.discardActiveObject();
      this.fabricjsService.canvas.requestRenderAll();
      this.figureService.addFigure(type, pointer.x, pointer.y, this.fabricjsService.canvas, this.idCounter);
      ++this.idCounter;
    }
  }
}