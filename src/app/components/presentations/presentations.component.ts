import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { PresentationService } from '../../services/presentation/presentation.service';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentations.component.html',
  styleUrls: ['./presentations.component.scss']
})

export class PresentationsComponent implements OnInit {

  constructor(
    private presentationService: PresentationService,
    private modalService: BsModalService,
    private router: Router
  ) { }

  presentations: any[];
  modalRef: BsModalRef;
  presentationOnDeleting: number;

  ngOnInit() {
    this.getPresentations();
  }

  getPresentations = () => {
    this.presentationService.getPresentations()
      .subscribe(
        presentations => {
          this.presentations = presentations;
        }
      );
  }

  deletePresentation = (id: number) => {
    this.presentationService.deletePresentation(id)
      .subscribe(
        successCode => {
          this.getPresentations();
        },
        errorCode => alert('Error in deleting')
      );
  }

  openModal = (template: any, id: number): void => {
    this.modalRef = this.modalService.show(template);
    this.presentationOnDeleting = id;
  }

  confirm = (): void => {
    this.deletePresentation(this.presentationOnDeleting);
    this.modalRef.hide();
  }

  decline = (): void => {
    this.modalRef.hide();
    this.presentationOnDeleting = null;
  }

  navigateToView = (id: number): void => {
    this.router.navigate(['/view', id]);
  }

  navigateToEdit = (id: number): void => {
    this.router.navigate(['/edit', id]);
  }

  navigateToAdd = (): void => {
    this.router.navigate(['/add']);
  }
}