import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import 'fabric';
declare let fabric;

import { Constants } from '../../classes/constants'
import { PresentationService } from '../../services/presentation/presentation.service';
import { FabricjsService } from '../../services/fabricjs/fabricjs.service';
import { AnimationService } from '../../services/fabric-objects/animation/animation.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})

export class EditorComponent implements OnInit, OnDestroy, AfterViewInit {
  private sub: any;
  addSlideForm: FormGroup;
  addPresentationForm: FormGroup;
  slideTitle: FormControl;
  presentationTitleControl: FormControl;
  modalRef: BsModalRef;
  indexOfEditedSlide: number;
  addingNewSlide = false;
  editingSlide = false;
  performingEdit = false;
  submitSlideTry = false;
  submitPresentationTry = false;
  addingPresentation = false;
  editingPresentation = false;
  showingSlideContent = true;
  presentation: any = {};
  activeSlide: any;

  @ViewChild('templateCancelSlide') templateCancelSlide: ElementRef;

  constructor(
    private modalService: BsModalService,
    private presentationService: PresentationService,
    private fabricjsService: FabricjsService,
    private animationService: AnimationService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.createFormControls();
    this.createForms();
    this.sub = this.route.params.subscribe(params => {
      const id = +params['id'];
      if (isNaN(id)) {
        this.addingPresentation = true;
        this.presentation.title = 'New presentation';
        this.presentation.slides = [];
        this.activeSlide = {
          id: this.presentation.slides.length,
          title: `Slide ${this.presentation.slides.length + 1}`,
          figures: [],
          texts: [],
          images: [],
          animations: [],
          background: {
            color: '',
            image: ''
          }
        }
        this.presentation.slides.push(this.activeSlide);
        this.presentation.slides[0].isActive = true;
      } else {
        this.getPresentation(id);
        this.editingPresentation = true;
      }
    });
  }

  ngAfterViewInit() {
    this.fabricjsService.initCanvas();
    if (this.addingPresentation) {
      this.showCurrentSlide(0, true);
    }
  }
l
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  createFormControls = (): void => {
    this.slideTitle = new FormControl('', Validators.required);
    this.presentationTitleControl = new FormControl('', Validators.required);
  }

  createForms = (): void => {
    this.addSlideForm = new FormGroup({
      slideTitle: this.slideTitle
    });
    this.addPresentationForm = new FormGroup({
      presentationTitle: this.presentationTitleControl
    });
  }

  saveSlide = (): void => {
    if (this.addSlideForm.valid) {
      if (this.editingSlide) {
        this.fabricjsService.canvas.forEachObject(object => {
          object.selectable = false;
        });
        this.fabricjsService.getSlideState();
        this.presentation.slides[this.indexOfEditedSlide] = JSON.parse(JSON.stringify(this.fabricjsService.editedSlide));
        this.fabricjsService.editedSlide = {};
        this.activeSlide = JSON.parse(JSON.stringify(this.presentation.slides[this.indexOfEditedSlide]));
      }
      this.submitSlideTry = false;
      this.indexOfEditedSlide = null;
      this.performingEdit = false;
      if (this.determineSlideEmptiness()) {
        this.createEmptySlideMessage();
        this.switchView('show');
      } else {
        this.setSlide(this.activeSlide, true);
      }
    }
  }

  getMaxSlideId = (): number => {
    let maxId;
    if (this.presentation.slides.length) {
      maxId = this.presentation.slides[0].id;
      for (let i = 0; i < this.presentation.slides.length; ++i) {
        if (this.presentation.slides[i].id > maxId) {
          maxId = this.presentation.slides[i].id;
        }
      }
    } else {
      maxId = 0;
    }
    return maxId;
  }

  addSlide = (): void => {
    this.fabricjsService.canvas.clear();
    for (let i = 0; i < this.presentation.slides.length; ++i) {
      if (this.presentation.slides[i].isActive) {
        this.presentation.slides[i].isActive = false;
        break;
      }
    }
    this.activeSlide = {
      id: this.getMaxSlideId() + 1,
      title: `Slide ${this.presentation.slides.length + 1}`,
      background: {
        color: '',
        image: ''
      },
      figures: [],
      texts: [],
      images: [],
      animations: []
    }
    this.presentation.slides.push(this.activeSlide);
    this.showCurrentSlide(this.presentation.slides.length - 1, true);
  }

  editSlide = (index: number): void => {
    this.showCurrentSlide(index, false);
    this.fabricjsService.editedSlide = JSON.parse(JSON.stringify(this.activeSlide));
    if (this.determineSlideEmptiness()) {
      this.fabricjsService.canvas.clear();
      this.fabricjsService.canvas.requestRenderAll();
    }
    this.indexOfEditedSlide = index;
    this.performingEdit = true;
    this.switchView('edit');
    for (let i = 0; i < this.presentation.slides.length; ++i) {
      if (i === index) {
        this.presentation.slides[i].isActive = true;
      } else {
        this.presentation.slides[i].isActive = false;
      }
    }
    this.fabricjsService.typeOfEditing.list = true;
  }

  deleteSlide = (index: number): void => {
    if (this.presentation.slides[index].isActive === true) {
      if (index === 0 && this.presentation.slides.length > 1) {
        this.presentation.slides[1].isActive = true;
        this.showCurrentSlide(1, true);
      } else {
        if (this.presentation.slides.length > 1) {
          this.presentation.slides[0].isActive = true;
          this.showCurrentSlide(0, true);
        }
      }
    }
    this.presentation.slides.splice(index, 1);
    if (this.presentation.slides.length === 0) {
      this.addSlide();
    }
  }

  showCurrentSlide = (index: number, forView: boolean): void => {
    if (!this.addingNewSlide) {
      this.activeSlide = JSON.parse(JSON.stringify(this.presentation.slides[index]));
      if (this.determineSlideEmptiness()) {
        this.createEmptySlideMessage();
      } else {
        this.setSlide(this.activeSlide, forView);
      }
      this.setActiveListItem(this.activeSlide.id);
      this.switchView('show');
    }
  }

  determineSlideEmptiness = (): boolean => {
    return !(
      this.activeSlide.background.color ||
      this.activeSlide.background.image ||
      this.activeSlide.figures.length ||
      this.activeSlide.images.length ||
      this.activeSlide.texts.length
    );
  }

  createEmptySlideMessage = () => {
    this.fabricjsService.canvas.clear();
    const message = new fabric.IText('Empty slide', {
      left: this.fabricjsService.canvas.width / 2,
      top: this.fabricjsService.canvas.height / 2,
      fontFamily: 'Tahoma',
      fontSize: 60,
      fill: 'rgb(100, 100, 100)'
    });
    this.fabricjsService.canvas.add(message);
    this.fabricjsService.canvas.requestRenderAll();
  }

  changeTypeOfEditing = (type: string) => {
    for (const key in this.fabricjsService.typeOfEditing) {
      this.fabricjsService.typeOfEditing[key] = false;
    }
    this.fabricjsService.typeOfEditing[type] = true;
  }

  initElementAdding = (type: string) => {
    this.fabricjsService.componentRole = 'add';
    this.fabricjsService.getSlideState();
    this.changeTypeOfEditing(type);
    this.fabricjsService.canvas.forEachObject(object => {
      object.selectable = false;
      if (object.type === 'i-text') {
        object.editable = false;
      }
    });
    if (this.fabricjsService.canvas.getActiveObject()) {
      this.fabricjsService.canvas.discardActiveObject(this.fabricjsService.canvas.getActiveObject());
      this.fabricjsService.canvas.requestRenderAll();
    }
    this.fabricjsService.canvas.on('selection:updated', this.fabricjsService.getActiveObject);
    this.fabricjsService.canvas.on('selection:created', this.fabricjsService.getActiveObject);
  }

  initSlidePropertiesMode = () => {
    this.fabricjsService.getSlideState();
    this.changeTypeOfEditing('other');
    this.fabricjsService.canvas.discardActiveObject();
    this.fabricjsService.canvas.forEachObject(object => {
      object.selectable = false;
      if (object.type === 'i-text') {
        object.editable = false;
      }
    });
    this.fabricjsService.canvas.requestRenderAll();
  }

  initAnimationMode = () => {
    this.fabricjsService.getSlideState();
    this.changeTypeOfEditing('animation');
    this.fabricjsService.canvas.forEachObject((object) => {
      object.lockMovementX = true;
      object.lockMovementY = true;
      object.lockScalingX = true;
      object.lockScalingY = true;
      object.lockRotation = true;
      if (object.type === 'i-text') {
        object.editable = false;
      }
    });
    this.fabricjsService.canvas.on('selection:updated', this.fabricjsService.getActiveObjectAnimations);
    this.fabricjsService.canvas.on('selection:created', this.fabricjsService.getActiveObjectAnimations);
  }

  setSlide = (slide: any, forView) => {
    this.fabricjsService.addExistingObjectsToCanvas(slide, null, forView);
    if (!this.performingEdit) {
      this.switchView('show');
      this.setActiveListItem(slide.id);
    }
  }

  setActiveListItem = (id: number) => {
    for (let i = 0; i < this.presentation.slides.length; ++i) {
      this.presentation.slides[i].isActive = false;
      if (this.presentation.slides[i].id === id) {
        this.presentation.slides[i].isActive = true;
      }
    }
  }

  switchView = (mode: string): void => {
    switch (mode) {
      case 'add':
        this.addingNewSlide = true;
        this.editingSlide = false;
        this.showingSlideContent = false;
        break;
      case 'edit':
        this.addingNewSlide = false;
        this.editingSlide = true;
        this.showingSlideContent = false;
        this.fabricjsService.toggleObjectsSelectability('all', true);
        break;
      case 'show':
        this.addingNewSlide = false;
        this.editingSlide = false;
        this.showingSlideContent = true;
        this.fabricjsService.toggleObjectsSelectability('all', false);
        break;
      default:
        break;
    }
  }

  cancel = () => {
    if (!this.fabricjsService.typeOfEditing.list) {
      for (let key in this.fabricjsService.typeOfEditing) {
        this.fabricjsService.typeOfEditing[key] = false;
      }
      this.fabricjsService.typeOfEditing.list = true;
      this.cancelElementChanges();
      this.performingEdit = true;
      this.setSlide(this.activeSlide, false);
      this.fabricjsService.canvas.forEachObject(object => {
        object.selectable = true;
        if (object.type === 'i-text') {
          object.editable = true;
        }
      });
      this.fabricjsService.canvas.off('selection:created');
      this.fabricjsService.canvas.off('selection:updated');
      this.fabricjsService.canvas.off('selection:cleared');
    } else {
      this.openModalCancel(this.templateCancelSlide);
    }

  }

  cancelElementChanges = () => {
    this.activeSlide = JSON.parse(JSON.stringify(this.fabricjsService.editedSlide));
    switch (this.fabricjsService.editedElement.type) {
      case 'i-text':
        for (let i = 0; i < this.activeSlide.texts.length; ++i) {
          if (this.fabricjsService.editedElement.id === this.activeSlide.texts[i].id) {
            this.activeSlide.texts[i] = {};
            this.activeSlide.texts[i] = JSON.parse(JSON.stringify(this.fabricjsService.editedElement));
            break;
          }
        }
        break;
      case 'image':
        for (let i = 0; i < this.activeSlide.images.length; ++i) {
          if (this.fabricjsService.editedElement.id === this.activeSlide.images[i].id) {
            this.activeSlide.images[i] = {};
            this.activeSlide.images[i] = JSON.parse(JSON.stringify(this.fabricjsService.editedElement));
            break;
          }
        }
        break;
      default:
        for (let i = 0; i < this.activeSlide.figures.length; ++i) {
          if (this.fabricjsService.editedElement.id === this.activeSlide.figures[i].id) {
            this.activeSlide.figures[i] = {};
            this.activeSlide.figures[i] = JSON.parse(JSON.stringify(this.fabricjsService.editedElement));
            break;
          }
        }
        break;
    }
  }

  savePresentation = (): void => {
    for (let i = 0; i < this.presentation.slides.length; ++i) {
      delete this.presentation.slides[i].isActive;
    }
    if (this.editingPresentation) {
      this.presentationService.editPresentation(this.presentation)
        .subscribe(successCode => {
          this.router.navigate(['/presentations']);
        },
          errorCode => alert('Error in editing')
        );
    }

    if (this.addingPresentation) {
      this.presentationService.addPresentation(this.presentation)
        .subscribe(successCode => {
          this.router.navigate(['/presentations']);
        },
          errorCode => alert('Error in adding')
        );
    }
  }

  openModalSave = (template: any): void => {
    if (this.addPresentationForm.valid) {
      this.modalRef = this.modalService.show(template);
    }
    this.submitPresentationTry = true;
  }

  openModalCancel = (template: any): void => {
    this.modalRef = this.modalService.show(template);
  }

  confirmSave = (): void => {
    this.modalRef.hide();
    this.savePresentation();
  }

  confirmCancelSlide = () => {
    this.modalRef.hide();
    this.performingEdit = false;
    this.activeSlide = JSON.parse(JSON.stringify(this.presentation.slides[this.indexOfEditedSlide]));
    if (this.determineSlideEmptiness()) {
      this.createEmptySlideMessage();
    } else {
      this.setSlide(this.activeSlide, true);
    }
    this.submitSlideTry = false;
    this.indexOfEditedSlide = null;
    this.fabricjsService.editedSlide = {};
    this.switchView('show');
  }

  confirmCancelPresentation = () => {
    this.modalRef.hide();
    this.navigateToMain();
  }

  navigateToMain = (): void => {
    this.router.navigate(['/presentations']);
  }

  getPresentation = (id: number): void => {
    this.presentationService.getPresentation(id).subscribe(presentation => {
      this.presentation = presentation;
      this.showCurrentSlide(0, true);
      this.switchView('show');
    })
  }
}