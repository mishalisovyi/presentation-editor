import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FabricjsService } from '../../services/fabricjs/fabricjs.service';
import { TextService } from '../../services/fabric-objects/text/text.service';
import { FigureService } from '../../services/fabric-objects/figure/figure.service';
import { ImageService } from '../../services/fabric-objects/image/image.service';
import { AnimationService } from '../../services/fabric-objects/animation/animation.service';

@Component({
  selector: 'app-elements-list',
  templateUrl: './elements-list.component.html',
  styleUrls: ['./elements-list.component.scss']
})
export class ElementsListComponent implements OnInit, AfterViewInit {

  slideElements: any[] = []

  constructor(
    private fabricjsService: FabricjsService,
    private textService: TextService,
    private figureService: FigureService,
    private imageService: ImageService,
    private animationService: AnimationService
  ) { }

  ngOnInit() {
    this.getSlideElements();
    this.changeTypeOfEditing('list');
  }

  ngAfterViewInit() {
    this.fabricjsService.canvas.on('selection:updated', this.setListItemActive);
    this.fabricjsService.canvas.on('selection:created', this.setListItemActive);
    this.fabricjsService.canvas.on('selection:cleared', this.setListItemNonActive);
  }

  getSlideElements = () => {
    this.slideElements.length = 0;
    for (let i = 0; i < this.fabricjsService.editedSlide.texts.length; ++i) {
      this.fabricjsService.editedSlide.texts[i].isActive = false;
      this.slideElements.push(JSON.parse(JSON.stringify(this.fabricjsService.editedSlide.texts[i])));
    }
    for (let i = 0; i < this.fabricjsService.editedSlide.figures.length; ++i) {
      this.fabricjsService.editedSlide.figures[i].isActive = false;
      this.slideElements.push(JSON.parse(JSON.stringify(this.fabricjsService.editedSlide.figures[i])));
    }
    for (let i = 0; i < this.fabricjsService.editedSlide.images.length; ++i) {
      this.fabricjsService.editedSlide.images[i].isActive = false;
      this.slideElements.push(JSON.parse(JSON.stringify(this.fabricjsService.editedSlide.images[i])));
    }
  }

  setListItemActive = (e: any) => {
    this.setListItemNonActive();
    for (let i = 0; i < this.slideElements.length; ++i) {
      if (this.slideElements[i].type === e.target.type && this.slideElements[i].id === e.target.id) {
        this.slideElements[i].isActive = true;
        break;
      }
    }
  }

  setListItemNonActive = () => {
    this.slideElements.forEach(element => {
      element.isActive = false;
    })
  }

  setSlideElementActive = (id: number, type: string) => {
    this.setListItemNonActive();
    for (let i = 0; i < this.slideElements.length; ++i) {
      if (this.slideElements[i].type === type && this.slideElements[i].id === id) {
        this.slideElements[i].isActive = true;
        break;
      }
    }
    for (let i = 0; i < this.fabricjsService.canvas._objects.length; ++i) {
      if (this.fabricjsService.canvas._objects[i].id === id && this.fabricjsService.canvas._objects[i].type === type) {
        this.fabricjsService.canvas.setActiveObject(this.fabricjsService.canvas._objects[i]);
        break;
      }
    }
    this.fabricjsService.canvas.requestRenderAll();
  }

  deleteElement = (id: number, type: string) => {
    this.setSlideElementActive(id, type);
    this.fabricjsService.canvas.remove(this.fabricjsService.canvas.getActiveObject());
    for (let i = 0; i < this.slideElements.length; ++i) {
      if (this.slideElements[i].id === id && this.slideElements[i].type === type) {
        this.slideElements.splice(i, 1);
        break;
      }
    }
    let deletedAnimationsIds: number[] = [];
    for (let i = this.fabricjsService.editedSlide.animations.length - 1; i >= 0; --i) {
      if (this.fabricjsService.editedSlide.animations[i].hostId === id &&
        this.fabricjsService.editedSlide.animations[i].hostType === type
      ) {
        deletedAnimationsIds.push(this.fabricjsService.editedSlide.animations[i].id);
        this.fabricjsService.editedSlide.animations.splice(i, 1);
      }
    }
    this.animationService.correctStartAfter(this.fabricjsService, deletedAnimationsIds);
    this.animationService.correctNextAnimations(this.fabricjsService, deletedAnimationsIds);
  }

  changeTypeOfEditing = (type: string) => {
    for (const key in this.fabricjsService.typeOfEditing) {
      this.fabricjsService.typeOfEditing[key] = false;
    }
    this.fabricjsService.typeOfEditing[type] = true;
    if (type !== 'text' && type !== 'list') {
      this.fabricjsService.canvas.forEachObject(obj => {
        if (obj.type === 'i-text') {
          obj.editable = false;
        }
      })
    }
  }

  editElement = (element: any) => {
    this.fabricjsService.componentRole = 'edit';
    this.fabricjsService.canvas.on('selection:updated', this.fabricjsService.getActiveObject);
    this.fabricjsService.canvas.on('selection:created', this.fabricjsService.getActiveObject);
    this.setSlideElementActive(element.id, element.type);
    this.fabricjsService.getSlideState();
    switch (element.type) {
      case 'i-text':
        this.textService.getActiveText(this.fabricjsService.canvas.getActiveObject());
        this.fabricjsService.editedElement = JSON.parse(JSON.stringify(this.textService.activeText));
        this.changeTypeOfEditing('text');
        break;
      case 'image':
        this.imageService.getActiveImage(this.fabricjsService.canvas.getActiveObject());
        this.fabricjsService.editedElement = JSON.parse(JSON.stringify(this.imageService.activeImage));
        this.changeTypeOfEditing('images');
        break;
      default:
        this.figureService.getActiveFigure(this.fabricjsService.canvas.getActiveObject());
        this.fabricjsService.editedElement = JSON.parse(JSON.stringify(this.figureService.activeFigure));
        this.changeTypeOfEditing('figures');
        break;
    }
    this.fabricjsService.editedElement.type = element.type;
    this.fabricjsService.canvas.forEachObject(object => {
      if (object.id !== element.id || object.type !== element.type) {
        object.selectable = false;
      }
    });
  }
}