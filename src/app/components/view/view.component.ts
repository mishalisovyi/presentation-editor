import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { WindowRefService } from '../../services/window-ref/window-ref.service';
import { FabricjsService } from '../../services/fabricjs/fabricjs.service';
import { AnimationService } from '../../services/fabric-objects/animation/animation.service';
import 'fabric';
declare let fabric: any;
declare let Reveal: any;

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})

export class ViewComponent implements OnInit, AfterViewInit {

  @ViewChild('slides') slides: ElementRef;

  presentation: any;
  activeSlideLink = [];
  menuIsOpened = false;
  activeSlide: any;
  private canvases: any;
  windowWidth: number;
  windowHeight: number;
  visibleButtons = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private windowRefService: WindowRefService,
    private fabricjsService: FabricjsService,
    private animationService: AnimationService
  ) { }

  ngOnInit() {
    this.presentation = this.route.snapshot.data['presentation'];
    this.setElementsScale(this.presentation);
    this.setFirstSlideLinkActive();
    this.windowWidth = this.windowRefService.getWindowWidth();
    this.windowHeight = this.windowRefService.getWindowHeight();
  }

  ngAfterViewInit() {
    this.canvases = new Array(this.presentation.slides.length);
    for (let i = 0; i < this.presentation.slides.length; ++i) {
      this.canvases[i] = new fabric.Canvas(`canvas${i}`, {
        width: this.windowWidth,
        height: this.windowHeight,
        selection: false,
        backgroundColor: this.presentation.slides[i].background.color
      });
      this.fabricjsService.setBackgroundImage(this.presentation.slides[i].background.image, this.canvases[i]);
    }
    fabric.Object.prototype.set({
      originX: 'center',
      originY: 'center'
    });
    this.fabricjsService.easeRef = fabric.util.ease;
    if (this.presentation.slides.length) {
      this.activeSlide = JSON.parse(JSON.stringify(this.presentation.slides[0]));
    }
    this.fabricjsService.addExistingObjectsToCanvas(this.activeSlide, this.canvases[0], true);
    this.playAnimations(0);
    Reveal.initialize({
      transition: 'slide',
      controls: true,
      progress: true,
      history: true,
      center: true
    });
    this.setSlideView();
    Reveal.addEventListener('slidechanged', (event) => {
      this.toggleActiveLinkClass(event.indexh);
      this.activeSlide = JSON.parse(JSON.stringify(this.presentation.slides[event.indexh]));
      this.fabricjsService.addExistingObjectsToCanvas(this.activeSlide, this.canvases[event.indexh], true);
      this.playAnimations(event.indexh);
    });
  }

  playAnimations = (slideIndex) => {
    setTimeout(() => {
      for (let i = 0; i < this.activeSlide.animations.length; ++i) {
        if (this.activeSlide.animations[i].startAfter < 0) {
          this.animationService.playAnimations(this.fabricjsService, this.activeSlide.animations[i], 'view', this.activeSlide.animations, this.canvases[slideIndex]);
        }
      }
    }, 400)
  }

  setSlideView = () => {
    for (let i = 0; i < this.slides.nativeElement.children.length; ++i) {
      if (this.slides.nativeElement.children[i].localName === 'section') {
        this.slides.nativeElement.children[i].style.cssText = 'top: 0px; display: block;';
      }
    }
    Reveal.slide(-1, -1, -1);
  }

  toggleMenu = () => {
    this.menuIsOpened = !this.menuIsOpened;
  }

  switchSlide = (index: number) => {
    Reveal.slide(index);
    this.toggleActiveLinkClass(index);
  }

  navigateToMain = () => {
    Reveal.removeEventListeners();
    this.router.navigate(['/presentations']);
  }

  setFirstSlideLinkActive = (): void => {
    let length = this.presentation.slides.length;
    for (let i = 0; i < length; ++i) {
      this.activeSlideLink.push(false);
    }
    this.activeSlideLink[0] = true;
  }

  toggleActiveLinkClass = (index: number): void => {
    let length = this.presentation.slides.length;
    for (let i = 0; i < length; ++i) {
      this.activeSlideLink[i] = false;
    }
    this.activeSlideLink[index] = true;
  }

  setElementsScale = (presentation: any) => {
    let item: any;
    const scaleCoef = 4 / 3;
    for (let i = 0; i < presentation.slides.length; ++i) {
      for (let j = 0; j < presentation.slides[i].figures.length; ++j) {
        presentation.slides[i].figures[j].left *= scaleCoef;
        presentation.slides[i].figures[j].top *= scaleCoef;
        presentation.slides[i].figures[j].scaleX *= scaleCoef;
        presentation.slides[i].figures[j].scaleY *= scaleCoef;
      }
      for (let j = 0; j < presentation.slides[i].texts.length; ++j) {
        presentation.slides[i].texts[j].left *= scaleCoef;
        presentation.slides[i].texts[j].top *= scaleCoef;
        presentation.slides[i].texts[j].scaleX *= scaleCoef;
        presentation.slides[i].texts[j].scaleY *= scaleCoef;
      }
      for (let j = 0; j < presentation.slides[i].images.length; ++j) {
        presentation.slides[i].images[j].left *= scaleCoef;
        presentation.slides[i].images[j].top *= scaleCoef;
        presentation.slides[i].images[j].scaleX *= scaleCoef;
        presentation.slides[i].images[j].scaleY *= scaleCoef;
      }
      for (let j = 0; j < presentation.slides[i].animations.length; ++j) {
        if (presentation.slides[i].animations[j].property === 'position') {
          presentation.slides[i].animations[j].value.x *= scaleCoef;
          presentation.slides[i].animations[j].value.y *= scaleCoef;
        }
        if (['scale', 'scaleX', 'scaleY'].indexOf(presentation.slides[i].animations[j].property) >= 0) {
          presentation.slides[i].animations[j].value.common *= scaleCoef;
        }
      }
    }
  }
}