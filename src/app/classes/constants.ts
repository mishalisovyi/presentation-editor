export class Constants {
  static get fontFamilies(): string[] {
    return [
      'Arial',
      'Arvo',
      'Bowlby One SC',
      'Coiny',
      'Impact',
      'Indie Flower',
      'Josefin Sans',
      'Lobster',
      'Poppins',
      'Tahoma',
      'Times New Roman'
    ];
  };
  static get fontSizes(): number[] {
    return [
      10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88
    ];
  };
  static get lineHeights(): number[] {
    return [
      0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2
    ];
  };
  static get animationProperties(): string[] {
    return [
      'position',
      'angle',
      'opacity',
      'scaleX',
      'scaleY',
      'scale'
    ];
  };
  static get easings(): any[] {
    return [
      {
        title: 'Very slow increase',
        value: 'easeInSine'
      },
      {
        title: 'Very slow decrease',
        value: 'easeOutSine'
      },
      {
        title: 'Very slow middle',
        value: 'easeInOutSine'
      },
      {
        title: 'Slow increase',
        value: 'easeInQuad'
      },
      {
        title: 'Slow decrease',
        value: 'easeOutQuad'
      },
      {
        title: 'Slow middle',
        value: 'easeInOutQuad'
      },
      {
        title: 'Half slow increase',
        value: 'easeInCubic'
      },
      {
        title: 'Half slow decrease',
        value: 'easeOutCubic'
      },
      {
        title: 'Half slow middle',
        value: 'easeInOutCubic'
      },
      {
        title: 'Medium increase',
        value: 'easeInQuart'
      },
      {
        title: 'Medium decrease',
        value: 'easeOutQuart'
      },
      {
        title: 'Medium middle',
        value: 'easeInOutQuart'
      },
      {
        title: 'Half quick increase',
        value: 'easeInQuint'
      },
      {
        title: 'Half quick decrease',
        value: 'easeOutQuint'
      },
      {
        title: 'Half quick middle',
        value: 'easeInOutQuint'
      },
      {
        title: 'Quick increase',
        value: 'easeInExpo'
      },
      {
        title: 'Quick decrease',
        value: 'easeOutExpo'
      },
      {
        title: 'Quick middle',
        value: 'easeInOutExpo'
      },
      {
        title: 'Very quick increase',
        value: 'easeInCirc'
      },
      {
        title: 'Very quick decrease',
        value: 'easeOutCirc'
      },
      {
        title: 'Very quick middle',
        value: 'easeInOutCirc'
      },
      {
        title: 'Back start',
        value: 'easeInBack'
      },
      {
        title: 'Back end',
        value: 'easeOutBack'
      },
      {
        title: 'Back both',
        value: 'easeInOutBack'
      },
      {
        title: 'Elastic start',
        value: 'easeInElastic'
      },
      {
        title: 'Elastic end',
        value: 'easeOutElastic'
      },
      {
        title: 'Elastic both',
        value: 'easeInOutElastic'
      },
      {
        title: 'Bounce start',
        value: 'easeInBounce'
      },
      {
        title: 'Bounce end',
        value: 'easeOutBounce'
      },
      {
        title: 'Bounce both',
        value: 'easeInOutBounce'
      }
    ]
  };
}