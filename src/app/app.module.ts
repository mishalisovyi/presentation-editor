import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularDraggableModule } from 'angular2-draggable';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { MatMenuModule } from '@angular/material/menu';
import { ColorPickerModule } from 'ngx-color-picker';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { AppComponent } from './app.component';
import { PresentationsComponent } from './components/presentations/presentations.component';
import { ViewComponent } from './components/view/view.component';
import { EditorComponent } from './components/editor/editor.component';
import { TextComponent } from './components/edition-types/text/text.component';
import { FiguresComponent } from './components/edition-types/figures/figures.component';
import { AnimationComponent } from './components/edition-types/animation/animation.component';
import { ImagesComponent } from './components/edition-types/images/images.component';
import { OtherComponent } from './components/edition-types/other/other.component';
import { ElementsListComponent } from './components/elements-list/elements-list.component';

import { PresentationService } from './services/presentation/presentation.service';
import { PresentationResolveService } from './services/presentation-resolve/presentation-resolve.service';
import { WindowRefService } from './services/window-ref/window-ref.service';
import { FabricjsService } from './services/fabricjs/fabricjs.service';
import { TextService } from './services/fabric-objects/text/text.service';
import { FigureService } from './services/fabric-objects/figure/figure.service';
import { ImageService } from './services/fabric-objects/image/image.service';
import { AnimationService } from './services/fabric-objects/animation/animation.service';

const appRoutes: Routes = [
  {
    path: 'presentations',
    component: PresentationsComponent
  },
  {
    path: 'view/:id',
    component: ViewComponent,
    resolve: {
      presentation: PresentationResolveService
    }
  },
  {
    path: 'edit/:id',
    component: EditorComponent
  },
  {
    path: 'add',
    component: EditorComponent
  },
  {
    path: '**',
    redirectTo: 'presentations'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PresentationsComponent,
    ViewComponent,
    EditorComponent,
    TextComponent,
    FiguresComponent,
    AnimationComponent,
    ImagesComponent,
    OtherComponent,
    ElementsListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AngularDraggableModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatMenuModule,
    ColorPickerModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    PresentationService,
    PresentationResolveService,
    WindowRefService,
    FabricjsService,
    TextService,
    FigureService,
    ImageService,
    AnimationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }