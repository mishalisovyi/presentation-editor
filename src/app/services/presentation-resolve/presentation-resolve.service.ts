import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { PresentationService } from '../presentation/presentation.service';


@Injectable()
export class PresentationResolveService implements Resolve<any> {

  constructor(private service: PresentationService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    Observable<any> | Promise<any> | any {
    let id = +route.params['id'];
    return this.service.getPresentation(id);
  }
}