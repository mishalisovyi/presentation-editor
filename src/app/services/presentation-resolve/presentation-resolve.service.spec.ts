import { TestBed, inject } from '@angular/core/testing';

import { PresentationResolveService } from './presentation-resolve.service';

describe('PresentationResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PresentationResolveService]
    });
  });

  it('should be created', inject([PresentationResolveService], (service: PresentationResolveService) => {
    expect(service).toBeTruthy();
  }));
});
