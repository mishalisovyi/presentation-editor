import { Injectable } from '@angular/core';
import 'fabric';
declare let fabric;

@Injectable()
export class FigureService {

  constructor() { }

  activeFigure = {
    id: null,
    type: "rect",
    fill: "rgb(0,0,0)",
    opacity: 1,
    left: 0,
    top: 0,
    scaleX: 1,
    scaleY: 1,
    angle: 0,
    title: '',
    zIndex: null
  }

  resetActiveFigure = () => {
    this.activeFigure.id = null;
    this.activeFigure.type = '';
    this.activeFigure.left = 0;
    this.activeFigure.top = 0;
    this.activeFigure.fill = 'rgb(0, 0, 0)';
    this.activeFigure.opacity = 1;
    this.activeFigure.scaleX = 1;
    this.activeFigure.scaleY = 1;
    this.activeFigure.angle = 0;
    this.activeFigure.title = '';
    this.activeFigure.zIndex = null;
  }

  getActiveFigure = (object: any) => {
    this.activeFigure.id = object.id;
    this.activeFigure.type = object.type;
    this.activeFigure.left = Math.round(object.left);
    this.activeFigure.top = Math.round(object.top);
    this.activeFigure.fill = object.fill;
    this.activeFigure.opacity = object.opacity;
    this.activeFigure.scaleX = parseFloat(object.scaleX.toFixed(2));
    this.activeFigure.scaleY = parseFloat(object.scaleY.toFixed(2));
    this.activeFigure.angle = Math.round(object.angle);
    this.activeFigure.title = object.title;
    this.activeFigure.zIndex = object.zIndex;
  }

  addFigure = (type: string, left: number, top: number, canvas: any, id: number): void => {
    if (!this.activeFigure.title) {
      this.activeFigure.title = this.getNewFigureTitle(type, canvas);
    }
    this.activeFigure.top = top;
    this.activeFigure.left = left;
    this.activeFigure.id = id;
    this.activeFigure.type = type;
    let newFabricFigure: any = {};
    switch (type) {
      case 'circle':
        newFabricFigure = new fabric.Circle({
          radius: 50,
          fill: 'rgb(0, 255, 0)'
        })
        break;
      case 'triangle':
        newFabricFigure = new fabric.Triangle({
          width: 100,
          height: 100,
          fill: 'rgb(255, 0, 0)'
        });
        break;
      case 'rect':
        newFabricFigure = new fabric.Rect({
          width: 100,
          height: 100,
          fill: 'rgb(0, 0, 255)'
        });
        break;
      default:
        break;
    }
    if (!this.activeFigure.angle || this.activeFigure.angle < 0 || this.activeFigure.angle > 360) {
      this.activeFigure.angle = 0;
    }
    if (!this.activeFigure.scaleX || this.activeFigure.scaleX < 0.05 || this.activeFigure.scaleX > 9) {
      this.activeFigure.scaleX = 1;
    }
    if (!this.activeFigure.scaleY || this.activeFigure.scaleY < 0.05 || this.activeFigure.scaleY > 9) {
      this.activeFigure.scaleY = 1;
    }
    if (!this.activeFigure.opacity || this.activeFigure.opacity < 0 || this.activeFigure.opacity > 1) {
      this.activeFigure.opacity = 1;
    }
    newFabricFigure.top = this.activeFigure.top;
    newFabricFigure.left = this.activeFigure.left;
    newFabricFigure.scaleX = this.activeFigure.scaleX;
    newFabricFigure.scaleY = this.activeFigure.scaleY;
    newFabricFigure.angle = this.activeFigure.angle;
    newFabricFigure.opacity = this.activeFigure.opacity;
    newFabricFigure.id = this.activeFigure.id;
    newFabricFigure.title = this.activeFigure.title;
    newFabricFigure.minScaleLimit = 0.2;
    newFabricFigure.on('deselected', () => {
      if (!newFabricFigure.title) {
        switch (newFabricFigure.type) {
          case 'rect':
            newFabricFigure.title = 'Untitled rectangle'
            break;
          case 'circle':
            newFabricFigure.title = 'Untitled circle'
            break;
          case 'triangle':
            newFabricFigure.title = 'Untitled triangle'
            break;
          default:
            break;
        }
      }
      this.resetActiveFigure();
    });
    canvas.add(newFabricFigure);
    this.resetActiveFigure();
  }

  getEditedFabricObject = (canvas: any) => {
    let editedObject: any;
    for (let i = 0; i < canvas._objects.length; ++i) {
      if (['rect', 'triangle', 'circle'].indexOf(canvas._objects[i].type) >= 0 && canvas._objects[i].id === this.activeFigure.id) {
        editedObject = canvas._objects[i];
        break;
      }
    }
    return editedObject;
  }

  getNewFigureTitle = (type: string, canvas: any): string => {
    let titleName: string;
    let countOfObjects = 0;
    switch (type) {
      case 'rect':
        titleName = 'Rectangle';
        break;
      case 'circle':
        titleName = 'Circle';
        break;
      case 'triangle':
        titleName = 'Triangle';
        break;
      default:
        break;
    }
    canvas.forEachObject((object) => {
      if (object.type === type) {
        ++countOfObjects;
      }
    });
    return `${titleName} ${countOfObjects + 1}`;
  }

  addExistingFigureToCanvas = (figure: any, canvas: any, forView: boolean): void => {
    let newFigure: any = {};
    switch (figure.type) {
      case 'circle':
        newFigure = new fabric.Circle({
          radius: 50,
          fill: 'rgb(0, 255, 0)'
        });
        break;
      case 'triangle':
        newFigure = new fabric.Triangle({
          width: 100,
          height: 100,
          fill: 'rgb(255, 0, 0)'
        });
        break;
      case 'rect':
        newFigure = new fabric.Rect({
          width: 100,
          height: 100,
          fill: 'rgb(0, 0, 255)'
        });
        break;
      default:
        break;
    }
    newFigure.top = figure.top;
    newFigure.left = figure.left;
    newFigure.scaleX = figure.scaleX;
    newFigure.scaleY = figure.scaleY;
    newFigure.angle = figure.angle;
    newFigure.fill = figure.fill;
    newFigure.opacity = figure.opacity;
    newFigure.id = figure.id;
    newFigure.selectable = false;
    newFigure.minScaleLimit = 0.2;
    newFigure.title = figure.title;
    if (forView) {
      newFigure.hasBorders = false;
      newFigure.hasControls = false;
    }
    canvas.add(newFigure);
    canvas.requestRenderAll();
  }

  changeRotation = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeFigure.angle < 0 || this.activeFigure.angle > 360) {
        this.activeFigure.angle = 0;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('angle', this.activeFigure.angle).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeFigure.angle = value;
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('angle', this.activeFigure.angle).setCoords();
      canvas.requestRenderAll();
    }
  }

  changeWidth = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeFigure.scaleX < 0.05 || this.activeFigure.scaleX > 9) {
        this.activeFigure.scaleX = 1;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('scaleX', this.activeFigure.scaleX).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeFigure.scaleX = parseFloat(value.toFixed(2));
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('scaleX', this.activeFigure.scaleX).setCoords();
      canvas.requestRenderAll();
    }
  }

  changeHeight = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeFigure.scaleY < 0.05 || this.activeFigure.scaleY > 9) {
        this.activeFigure.scaleY = 1;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('scaleY', this.activeFigure.scaleY).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeFigure.scaleY = parseFloat(value.toFixed(2));
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('scaleY', this.activeFigure.scaleY).setCoords();
      canvas.requestRenderAll();
    }
  }

  changeOpacity = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeFigure.opacity < 0 || this.activeFigure.opacity > 1) {
        this.activeFigure.opacity = 1;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('opacity', this.activeFigure.opacity).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeFigure.opacity = parseFloat(value.toFixed(2));
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('opacity', this.activeFigure.opacity).setCoords();
      canvas.requestRenderAll();
    }
  }

  changeFill = (canvas: any) => {
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().fill = this.activeFigure.fill;
      canvas.getActiveObject().objectCaching = false;
    }
    canvas.requestRenderAll();
  }
}