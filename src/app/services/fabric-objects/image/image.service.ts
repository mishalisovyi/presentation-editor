import { Injectable } from '@angular/core';
import 'fabric';
declare let fabric;

@Injectable()
export class ImageService {

  activeImage: any = {};
  isActiveImage: any[] = [];
  isChangingImage = false;
  addImageTry = false;

  constructor() { }

  addExistingImageToCanvas = (image: any, canvas: any, forView: boolean, componentRole: string) => {
    const props = image;
    fabric.Image.fromURL(image.body, (img) => {
      let myImg = img.set({
        left: props.left,
        top: props.top,
        scaleX: props.scaleX,
        scaleY: props.scaleY,
        angle: props.angle,
        id: props.id,
        title: props.title,
        opacity: props.opacity,
        zIndex: props.zIndex,
        minScaleLimit: 0.2,
        type: 'image'
      });
      if (forView) {
        myImg.selectable = false;
      }
      if (forView) {
        myImg.hasControls = false;
        myImg.hasBorders = false;
      }
      if (componentRole === 'add') {
        myImg.on('deselected', () => {
          if (!myImg.title) {
            myImg.title = 'Untitled image';
          }
          if (!this.isChangingImage) {
            this.resetActiveImage();
          }
        });
      }
      canvas.add(myImg);
      canvas.moveTo(myImg, props.zIndex);
      this.resetActiveImage();
      if (this.isChangingImage) {
        canvas.setActiveObject(myImg);
      }
      canvas.requestRenderAll();
      this.isChangingImage = false;
    });
    this.addImageTry = false;
  }

  getEditedFabricObject = (canvas: any) => {
    let editedObject: any;
    for (let i = 0; i < canvas._objects.length; ++i) {
      if (canvas._objects[i].type === 'image' && canvas._objects[i].id === this.activeImage.id) {
        editedObject = canvas._objects[i];
        break;
      }
    }
    return editedObject;
  }

  getActiveImage = (image: any) => {
    this.activeImage.id = image.id;
    this.activeImage.body = image._element.currentSrc;
    this.activeImage.left = Math.round(image.left);
    this.activeImage.top = Math.round(image.top);
    this.activeImage.angle = Math.round(image.angle);
    this.activeImage.scaleX = parseFloat(image.scaleX.toFixed(2));
    this.activeImage.scaleY = parseFloat(image.scaleY.toFixed(2));
    this.activeImage.opacity = image.opacity;
    this.activeImage.title = image.title;
    this.activeImage.type = image.type;
    this.activeImage.zIndex = image.zIndex;
  }

  resetActiveImage = () => {
    this.activeImage.id = null;
    this.activeImage.left = 0;
    this.activeImage.top = 0;
    this.activeImage.body = '';
    this.activeImage.opacity = 1;
    this.activeImage.scaleX = 1;
    this.activeImage.scaleY = 1;
    this.activeImage.angle = 0;
    this.activeImage.title = '';
    this.activeImage.type = 'image';
    this.activeImage.zIndex = null;
  }

  changeRotation = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeImage.angle < 0 || this.activeImage.angle > 360) {
        this.activeImage.angle = 0;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('angle', this.activeImage.angle).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeImage.angle = value;
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('angle', this.activeImage.angle).setCoords();
      canvas.requestRenderAll();
    }
  }

  changeWidth = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeImage.scaleX < 0.05 || this.activeImage.scaleX > 9) {
        this.activeImage.scaleX = 1;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('scaleX', this.activeImage.scaleX).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeImage.scaleX = parseFloat(value.toFixed(2));
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('scaleX', this.activeImage.scaleX).setCoords();
      canvas.requestRenderAll();
    }
  }

  changeHeight = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeImage.scaleY < 0.05 || this.activeImage.scaleY > 9) {
        this.activeImage.scaleY = 1;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('scaleY', this.activeImage.scaleY).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeImage.scaleY = parseFloat(value.toFixed(2));
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('scaleY', this.activeImage.scaleY).setCoords();
      canvas.requestRenderAll();
    }
  }

  changeOpacity = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeImage.opacity < 0 || this.activeImage.opacity > 1) {
        this.activeImage.opacity = 1;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('opacity', this.activeImage.opacity).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeImage.opacity = parseFloat(value.toFixed(2));
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('opacity', this.activeImage.opacity).setCoords();
      canvas.requestRenderAll();
    }
  }
}