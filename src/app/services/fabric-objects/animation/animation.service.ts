import { Injectable } from '@angular/core';
import { Constants } from '../../../classes/constants';
import { FabricjsService } from '../../fabricjs/fabricjs.service';

@Injectable()
export class AnimationService {

  activeObjectAnimations: any[] = [];
  activeAnimation: any;
  propertiesBeforeAnimation: any[] = [];
  isChangedAnimation: boolean = false;
  isAddingAnimation: boolean = false;
  playingAnimation: boolean = false;
  countOfActiveAnimations: number = 0;
  stopAnimation: boolean = false;

  constructor() { }

  playSingleAnimation = (fabricjsService: any, fromEdit: boolean, id: number) => {
    if (!fromEdit) {
      for (let i = 0; i < this.activeObjectAnimations.length; ++i) {
        if (this.activeObjectAnimations[i].id === id) {
          this.activeAnimation = JSON.parse(JSON.stringify(this.activeObjectAnimations[i]));
          break;
        }
      }
    }
    fabricjsService.preserveObjectStacking = true;
    this.playingAnimation = true;
    let activeObject = fabricjsService.canvas.getActiveObject();
    activeObject.hasControls = false;
    activeObject.hasBorders = false;
    fabricjsService.canvas.requestRenderAll();
    let propertiesBeforeAnimation: any = {};
    propertiesBeforeAnimation.left = activeObject.left;
    propertiesBeforeAnimation.top = activeObject.top;
    propertiesBeforeAnimation.scaleX = activeObject.scaleX;
    propertiesBeforeAnimation.scaleY = activeObject.scaleY;
    propertiesBeforeAnimation.angle = activeObject.angle;
    propertiesBeforeAnimation.opacity = activeObject.opacity;
    switch (this.activeAnimation.property) {
      case 'position':
        if (fabricjsService.pointer.x && fabricjsService.pointer.y) {
          this.activeAnimation.value.x = fabricjsService.pointer.x;
          this.activeAnimation.value.y = fabricjsService.pointer.y;
        }
        activeObject.animate({ 'left': this.activeAnimation.value.x, 'top': this.activeAnimation.value.y }, {
          duration: this.activeAnimation.duration,
          onChange: fabricjsService.canvas.renderAll.bind(fabricjsService.canvas),
          onComplete: () => {
            fabricjsService.preserveObjectStacking = false;
            this.retrieveObjectAfterAnimation(fabricjsService.canvas, propertiesBeforeAnimation);
            this.stopAnimation = false;
          },
          abort: () => {
            return this.stopAnimation;
          },
          easing: fabricjsService.easeRef[this.activeAnimation.easing]
        });
        break;
      case 'scale':
        activeObject.animate({ 'scaleX': this.activeAnimation.value.common, 'scaleY': this.activeAnimation.value.common }, {
          duration: this.activeAnimation.duration,
          onChange: fabricjsService.canvas.renderAll.bind(fabricjsService.canvas),
          onComplete: () => {
            fabricjsService.preserveObjectStacking = false;
            this.retrieveObjectAfterAnimation(fabricjsService.canvas, propertiesBeforeAnimation);
            this.stopAnimation = false;
          },
          abort: () => {
            return this.stopAnimation;
          },
          easing: fabricjsService.easeRef[this.activeAnimation.easing]
        });
        break;
      default:
        activeObject.animate(this.activeAnimation.property, this.activeAnimation.value.common, {
          duration: this.activeAnimation.duration,
          onChange: fabricjsService.canvas.renderAll.bind(fabricjsService.canvas),
          onComplete: () => {
            fabricjsService.preserveObjectStacking = false;
            this.retrieveObjectAfterAnimation(fabricjsService.canvas, propertiesBeforeAnimation);
            this.stopAnimation = false;
          },
          abort: () => {
            return this.stopAnimation;
          },
          easing: fabricjsService.easeRef[this.activeAnimation.easing]
        });
        break;
    }
  }

  launchAnimations = (fabricjsService): void => {
    fabricjsService.canvas.forEachObject((object) => {
      object.hasControls = false;
      object.hasBorders = false;
    });
    this.playingAnimation = true;
    this.propertiesBeforeAnimation.length = 0;
    for (let i = 0; i < fabricjsService.canvas.getObjects().length; ++i) {
      let object = fabricjsService.canvas.item(i);
      let item = {
        left: object.left,
        top: object.top,
        scaleX: object.scaleX,
        scaleY: object.scaleY,
        angle: object.angle,
        opacity: object.opacity
      };
      this.propertiesBeforeAnimation.push(item);
    }
    for (let i = 0; i < fabricjsService.editedSlide.animations.length; ++i) {
      if (fabricjsService.editedSlide.animations[i].startAfter < 0) {
        this.playAnimations(fabricjsService, fabricjsService.editedSlide.animations[i], 'edit', null, null);
      }
    }
  }

  playAnimations = (fabricjsService: any, animation: any, mode: string, animations: any, canvas: any): void => {
    if (mode === 'edit') {
      canvas = fabricjsService.canvas;
      animations = JSON.parse(JSON.stringify(fabricjsService.editedSlide.animations));
    }
    canvas.preserveObjectStacking = true;
    fabricjsService.setActiveObjectByIdAndType(animation.hostId, animation.hostType, canvas);
    const activeObject = canvas.getActiveObject();
    activeObject.lockMovementX = true;
    activeObject.lockMovementY = true;
    activeObject.lockScalingX = true;
    activeObject.lockScalingY = true;
    activeObject.lockRotation = true;
    ++this.countOfActiveAnimations;
    switch (animation.property) {
      case 'position':
        canvas.getActiveObject().animate({ 'left': animation.value.x, 'top': animation.value.y }, {
          duration: animation.duration,
          onChange: canvas.renderAll.bind(canvas),
          onComplete: () => {
            --this.countOfActiveAnimations;
            if (!this.countOfActiveAnimations && !animation.nextAnimations.length) {
              canvas.preserveObjectStacking = false;
              this.stopAnimation = false;
              canvas.discardActiveObject();
              if (mode === 'edit') {
                this.retrieveSlideAfterAnimation(canvas, this.propertiesBeforeAnimation);
              }
            }
            else {
              if (animation.nextAnimations.length) {
                for (let i = 0; i < animation.nextAnimations.length; ++i) {
                  this.playAnimations(fabricjsService, this.getAnimationById(animations, animation.nextAnimations[i]), mode, animations, canvas);
                }
              }
            }
          },
          abort: () => {
            return this.stopAnimation;
          },
          easing: fabricjsService.easeRef[animation.easing]
        });
        break;
      case 'scale':
        canvas.getActiveObject().animate({ 'scaleX': animation.value.common, 'scaleY': animation.value.common }, {
          duration: animation.duration,
          onChange: canvas.renderAll.bind(canvas),
          onComplete: () => {
            --this.countOfActiveAnimations;
            if (!this.countOfActiveAnimations && !animation.nextAnimations.length) {
              canvas.preserveObjectStacking = false;
              this.stopAnimation = false;
              canvas.discardActiveObject();
              if (mode === 'edit') {
                this.retrieveSlideAfterAnimation(canvas, this.propertiesBeforeAnimation);
              }
            }
            else {
              if (animation.nextAnimations.length) {
                for (let i = 0; i < animation.nextAnimations.length; ++i) {
                  this.playAnimations(fabricjsService, this.getAnimationById(animations, animation.nextAnimations[i]), mode, animations, canvas);
                }
              }
            }
          },
          abort: () => {
            return this.stopAnimation;
          },
          easing: fabricjsService.easeRef[animation.easing]
        });
        break;
      default:
        canvas.getActiveObject().animate(animation.property, animation.value.common, {
          duration: animation.duration,
          onChange: canvas.renderAll.bind(canvas),
          onComplete: () => {
            --this.countOfActiveAnimations;
            if (!this.countOfActiveAnimations && !animation.nextAnimations.length) {
              canvas.preserveObjectStacking = false;
              this.stopAnimation = false;
              canvas.discardActiveObject();
              if (mode === 'edit') {
                this.retrieveSlideAfterAnimation(canvas, this.propertiesBeforeAnimation);
              }
            }
            else {
              if (animation.nextAnimations.length) {
                for (let i = 0; i < animation.nextAnimations.length; ++i) {
                  this.playAnimations(fabricjsService, this.getAnimationById(animations, animation.nextAnimations[i]), mode, animations, canvas);
                }
              }
            }
          },
          abort: () => {
            return this.stopAnimation;
          },
          easing: fabricjsService.easeRef[animation.easing]
        });
        break;
    }
  }

  correctStartAfter = (fabricjsService, id) => {
    for (let i = 0; i < fabricjsService.editedSlide.animations.length; ++i) {
      if (id === fabricjsService.editedSlide.animations[i].startAfter) {
        fabricjsService.editedSlide.animations[i].startAfter = -1;
        fabricjsService.editedSlide.animations[i].displayStartAfter = 'loading slide';
      }
    }
  }

  correctNextAnimations = (fabricjsService, id) => {
    for (let i = 0; i < fabricjsService.editedSlide.animations.length; ++i) {
      for (let j = fabricjsService.editedSlide.animations[i].nextAnimations.length - 1; j >= 0; --j) {
        if (fabricjsService.editedSlide.animations[i].nextAnimations[j] === id) {
          fabricjsService.editedSlide.animations[i].nextAnimations.splice(j, 1);
          break;
        }
      }
    }
  }

  getAnimationById = (animations: any, id: number): any => {
    let animation: any
    for (let i = 0; i < animations.length; ++i) {
      if (animations[i].id === id) {
        animation = animations[i];
        break;
      }
    }
    return animation;
  }

  retrieveObjectAfterAnimation = (canvas: any, startValues: any) => {
    let object = canvas.getActiveObject();
    object.set('left', startValues.left).setCoords();
    object.set('top', startValues.top).setCoords();
    object.set('scaleX', startValues.scaleX).setCoords();
    object.set('scaleY', startValues.scaleY).setCoords();
    object.set('angle', startValues.angle).setCoords();
    object.set('opacity', startValues.opacity).setCoords();
    object.hasControls = true;
    object.hasBorders = true;
    canvas.requestRenderAll();
    this.playingAnimation = false;
  }

  retrieveSlideAfterAnimation = (canvas: any, startValue: any) => {
    for (let i = 0; i < startValue.length; ++i) {
      let object = canvas.item(i);
      object.set('left', startValue[i].left).setCoords();
      object.set('top', startValue[i].top).setCoords();
      object.set('scaleX', startValue[i].scaleX).setCoords();
      object.set('scaleY', startValue[i].scaleY).setCoords();
      object.set('angle', startValue[i].angle).setCoords();
      object.set('opacity', startValue[i].opacity).setCoords();
      object.hasControls = true;
      object.hasBorders = true;
    }
    canvas.requestRenderAll();
    this.playingAnimation = false;
  }
}