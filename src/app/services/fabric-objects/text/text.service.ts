import { Injectable } from '@angular/core';
import * as FontFaceObserver from 'fontfaceobserver';
import { PresentationService } from '../../presentation/presentation.service';
import 'fabric';
declare let fabric;

@Injectable()
export class TextService {
  addTextTry: boolean = false;
  activeText = {
    id: null,
    left: 0,
    top: 0,
    body: '',
    fontFamily: 'Times New Roman',
    fontSize: 40,
    lineHeight: 1,
    textAlign: 'left',
    fontWeight: 'normal',
    fontStyle: 'normal',
    underline: false,
    linethrough: false,
    fill: 'rgb(0, 0, 0)',
    backgroundColor: 'rgb(255, 255, 255)',
    opacity: 1,
    scaleX: 1,
    scaleY: 1,
    angle: 0,
    title: '',
    type: 'i-text',
    zIndex: null
  }

  constructor(
    private presentationService: PresentationService
  ) { }

  resetActiveText = () => {
    this.activeText.id = null;
    this.activeText.left = 0;
    this.activeText.top = 0;
    this.activeText.body = '';
    this.activeText.fontFamily = 'Times New Roman';
    this.activeText.fontSize = 40;
    this.activeText.lineHeight = 1;
    this.activeText.textAlign = 'left';
    this.activeText.fontWeight = 'normal';
    this.activeText.fontStyle = 'normal';
    this.activeText.underline = false;
    this.activeText.linethrough = false;
    this.activeText.fill = 'rgb(0, 0, 0)';
    this.activeText.backgroundColor = 'rgb(255, 255, 255)';
    this.activeText.opacity = 1;
    this.activeText.scaleX = 1;
    this.activeText.scaleY = 1;
    this.activeText.angle = 0;
    this.activeText.title = '';
    this.activeText.type = 'i-text';
    this.activeText.zIndex = null;
  }

  getActiveText = (object: any) => {
    this.activeText.id = object.id;
    this.activeText.left = Math.round(object.left);
    this.activeText.top = Math.round(object.top);
    this.activeText.body = object.text;
    this.activeText.fontFamily = object.fontFamily;
    this.activeText.fontSize = object.fontSize;
    this.activeText.lineHeight = object.lineHeight;
    this.activeText.textAlign = object.textAlign;
    this.activeText.fontWeight = object.fontWeight;
    this.activeText.fontStyle = object.fontStyle;
    this.activeText.underline = object.underline;
    this.activeText.linethrough = object.linethrough;
    this.activeText.fill = object.fill;
    this.activeText.backgroundColor = object.backgroundColor;
    this.activeText.opacity = object.opacity;
    this.activeText.scaleX = parseFloat(object.scaleX.toFixed(2));
    this.activeText.scaleY = parseFloat(object.scaleY.toFixed(2));
    this.activeText.angle = Math.round(object.angle);
    this.activeText.title = object.title;
    this.activeText.type = object.type;
    this.activeText.zIndex = object.zIndex;
  }

  changeRotation = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeText.angle < 0 || this.activeText.angle > 360) {
        this.activeText.angle = 0;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('angle', this.activeText.angle).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeText.angle = value;
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('angle', this.activeText.angle).setCoords();
      canvas.requestRenderAll();
    }
  }

  changeWidth = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeText.scaleX < 0.05 || this.activeText.scaleX > 9) {
        this.activeText.scaleX = 1;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('scaleX', this.activeText.scaleX).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeText.scaleX = parseFloat(value.toFixed(2));
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('scaleX', this.activeText.scaleX).setCoords();
      canvas.requestRenderAll();
    }
  }

  changeHeight = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeText.scaleY < 0.05 || this.activeText.scaleY > 9) {
        this.activeText.scaleY = 1;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('scaleY', this.activeText.scaleY).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeText.scaleY = parseFloat(value.toFixed(2));
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('scaleY', this.activeText.scaleY).setCoords();
      canvas.requestRenderAll();
    }
  }

  addText = (canvas: any, id: number): void => {
    this.activeText.left = canvas.width / 2;
    this.activeText.top = canvas.height / 2;
    this.activeText.id = id;
    if (!this.activeText.angle || this.activeText.angle < 0 || this.activeText.angle > 360) {
      this.activeText.angle = 0;
    }
    if (!this.activeText.scaleX || this.activeText.scaleX < 0.05 || this.activeText.scaleX > 9) {
      this.activeText.scaleX = 1;
    }
    if (!this.activeText.scaleY || this.activeText.scaleY < 0.05 || this.activeText.scaleY > 9) {
      this.activeText.scaleY = 1;
    }
    if (!this.activeText.opacity || this.activeText.opacity < 0 || this.activeText.opacity > 1) {
      this.activeText.opacity = 1;
    }
    const text = Object.assign({}, this.activeText);
    let newText = new fabric.IText(text.body, text);
    newText.minScaleLimit = 1;
    newText.on('deselected', (e) => {
      if (!newText.text) {
        newText.set('text', 'Empty text');
        canvas.requestRenderAll();
      }
      if (!newText.title) {
        newText.title = 'Untitled text';
      }
      this.resetActiveText();
    });
    canvas.add(newText);
    this.setFontFamily('forNew', canvas);
    this.resetActiveText();
    this.addTextTry = false;
  }

  addExistingTextToCanvas = (text: any, canvas: any, forView: boolean): void => {
    const newText = new fabric.IText(text.body, {
      top: text.top,
      left: text.left,
      scaleX: text.scaleX,
      scaleY: text.scaleY,
      angle: text.angle,
      fill: text.fill,
      fontFamily: text.fontFamily,
      fontSize: text.fontSize,
      lineHeight: text.lineHeight,
      backgroundColor: text.backgroundColor,
      opacity: text.opacity,
      textAlign: text.textAlign,
      fontWeight: text.fontWeight,
      fontStyle: text.fontStyle,
      underline: text.underline,
      linethrough: text.linethrough,
      id: text.id,
      title: text.title,
      selectable: false,
      minScaleLimit: 1
    })
    if (forView) {
      newText.hasControls = false;
      newText.hasBorders = false;
    }
    canvas.add(newText);
    canvas.requestRenderAll();
  }

  setTextBody = (canvas: any, componentRole: string) => {
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('text', this.activeText.body);
      canvas.requestRenderAll();
    } else {
      if (componentRole === 'edit') {
        this.getEditedFabricObject(canvas).set('text', this.activeText.body);
        canvas.requestRenderAll();
      }
    }
  }

  getEditedFabricObject = (canvas: any) => {
    let editedObject: any;
    for (let i = 0; i < canvas._objects.length; ++i) {
      if (canvas._objects[i].type === 'i-text' && canvas._objects[i].id === this.activeText.id) {
        editedObject = canvas._objects[i];
        break;
      }
    }
    return editedObject;
  }

  setFontFamily = (_for: string, canvas: any): void => {
    if (['Arial', 'Impact', 'Tahoma', 'Times New Roman'].indexOf(this.activeText.fontFamily) === -1) {
      const myfont = new FontFaceObserver(this.activeText.fontFamily);
      myfont.load().then(() => {
        if (_for === 'forOld') {
          if (canvas.getActiveObject()) {
            canvas.getActiveObject().set('fontFamily', this.activeText.fontFamily);
          }
        } else {
          canvas.getObjects()[canvas.getObjects().length - 1].set('fontFamily', this.activeText.fontFamily);
        }
      }).catch(() => {
        alert('Unable to load font' + this.activeText.fontFamily);
      });
    } else {
      if (_for === 'forOld') {
        if (canvas.getActiveObject()) {
          canvas.getActiveObject().set('fontFamily', this.activeText.fontFamily);
        }
      } else {
        canvas.getObjects()[canvas.getObjects().length - 1].set('fontFamily', this.activeText.fontFamily);
      }
    }
    canvas.requestRenderAll();
  }

  setFontSize = (canvas: any): void => {
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('fontSize', this.activeText.fontSize);
      canvas.requestRenderAll();
    }
  }

  setLineHeight = (canvas): void => {
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('lineHeight', this.activeText.lineHeight);
      canvas.requestRenderAll();
    }
  }

  setAlign = (type: string, canvas: any): void => {
    this.activeText.textAlign = type;
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('textAlign', this.activeText.textAlign);
      canvas.requestRenderAll();
    }
  }

  toggleBold = (canvas: any): void => {
    if (this.activeText.fontWeight === 'normal') {
      this.activeText.fontWeight = 'bold';
    } else {
      this.activeText.fontWeight = 'normal'
    }
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('fontWeight', this.activeText.fontWeight);
      canvas.requestRenderAll();
    }
  }

  toggleItalic = (canvas: any): void => {
    if (this.activeText.fontStyle === 'italic') {
      this.activeText.fontStyle = 'normal';
    } else {
      this.activeText.fontStyle = 'italic'
    }
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('fontStyle', this.activeText.fontStyle);
      canvas.requestRenderAll();
    }
  }

  toggleUnderline = (canvas: any): void => {
    this.activeText.underline = !this.activeText.underline;
    if (canvas.getActiveObject()) {
      if (this.activeText.underline) {
        canvas.getActiveObject().set('underline', true);
      } else {
        canvas.getActiveObject().set('underline', false);
      }
      canvas.requestRenderAll();
    }
  }

  toggleStrike = (canvas: any): void => {
    this.activeText.linethrough = !this.activeText.linethrough;
    if (canvas.getActiveObject()) {
      if (this.activeText.linethrough) {
        canvas.getActiveObject().set('linethrough', true);
      } else {
        canvas.getActiveObject().set('linethrough', false);
      }
      canvas.requestRenderAll();
    }
  }

  changeFill = (canvas: any) => {
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().fill = this.activeText.fill;
      canvas.getActiveObject().objectCaching = false;
    }
    canvas.requestRenderAll();
  }

  changeBackgroundColor = (canvas: any) => {
    if (canvas.getActiveObject() && canvas.getActiveObject().type === 'i-text') {
      canvas.getActiveObject().backgroundColor = this.activeText.backgroundColor;
      canvas.getActiveObject().objectCaching = false;
    }
    canvas.requestRenderAll();
  }

  changeOpacity = (value: number, canvas: any): void => {
    if (value === -1) {
      if (this.activeText.opacity < 0 || this.activeText.opacity > 1) {
        this.activeText.opacity = 1;
      }
      if (canvas.getActiveObject()) {
        canvas.getActiveObject().set('opacity', this.activeText.opacity).setCoords();
        canvas.requestRenderAll();
      }
      return;
    }
    this.activeText.opacity = parseFloat(value.toFixed(2));
    if (canvas.getActiveObject()) {
      canvas.getActiveObject().set('opacity', this.activeText.opacity).setCoords();
      canvas.requestRenderAll();
    }
  }
}