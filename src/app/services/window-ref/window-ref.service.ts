import { Injectable } from '@angular/core';

@Injectable()
export class WindowRefService {

  constructor() { }

  getWindowHeight = (): any => {
    return window.innerHeight;
  }
  getWindowWidth = (): any => {
    return window.innerWidth;
  }
}