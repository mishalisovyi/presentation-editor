import { TestBed, inject } from '@angular/core/testing';

import { FabricjsService } from './fabricjs.service';

describe('FabricjsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FabricjsService]
    });
  });

  it('should be created', inject([FabricjsService], (service: FabricjsService) => {
    expect(service).toBeTruthy();
  }));
});
