import { Injectable } from '@angular/core';
import { WindowRefService } from '../../services/window-ref/window-ref.service';
import { TextService } from '../../services/fabric-objects/text/text.service';
import { FigureService } from '../../services/fabric-objects/figure/figure.service';
import { ImageService } from '../../services/fabric-objects/image/image.service';
import { AnimationService } from '../../services/fabric-objects/animation/animation.service';
import 'fabric';
declare let fabric;

@Injectable()
export class FabricjsService {
  canvas: any;
  easeRef: any;
  activeObjectPosition = {
    left: null,
    top: null,
    changePosition: false
  }
  pointer: any = {
    isLocked: true
  };
  typeOfEditing = {
    text: false,
    figures: false,
    animation: false,
    images: false,
    list: false,
    other: false
  }
  maxScale = 9;
  editedSlide: any = {};
  windowWidth: number = this.windowRefService.getWindowWidth();
  windowHeight: number = this.windowRefService.getWindowHeight();
  editedElement: any = {};
  componentRole = '';

  constructor(
    private windowRefService: WindowRefService,
    private textService: TextService,
    private figureService: FigureService,
    private imageService: ImageService,
    private animationService: AnimationService
  ) { }

  initCanvas = (): void => {
    this.canvas = new fabric.Canvas('canvas', {
      width: this.windowWidth * 0.75,
      height: this.windowHeight * 0.75,
      selection: false
    });
    fabric.Object.prototype.set({
      originX: 'center',
      originY: 'center'
    });
    this.easeRef = fabric.util.ease;
    this.canvas.on('text:changed', this.changeTextBody);
    this.canvas.on('mouse:up', this.createPointer);
    this.canvas.on('object:moving', (e: any) => {
      this.activeObjectPosition.left = e.target.left;
      this.activeObjectPosition.top = e.target.top;
      this.activeObjectPosition.changePosition = true;
      e.target.lastGoodTop = e.target.top;
      e.target.lastGoodLeft = e.target.left;
    });
    this.canvas.on('object:rotating', (e: any) => {
      switch (e.target.type) {
        case 'i-text':
          this.textService.activeText.angle = Math.floor(e.target.angle);
          break;
        case 'image':
          this.imageService.activeImage.angle = Math.floor(e.target.angle);
          break;
        default:
          this.figureService.activeFigure.angle = Math.floor(e.target.angle);
          break;
      }
    });
    this.canvas.on('object:scaling', (e: any) => {
      if (e.target.flipX === true || e.target.flipY === true) {
        e.target.flipX = false;
        e.target.flipY = false
      }
      if (e.target.scaleX > this.maxScale) {
        e.target.scaleX = this.maxScale;
        if (e.target.lastGoodLeft && e.target.lastGoodTop) {
          e.target.left = e.target.lastGoodLeft;
          e.target.top = e.target.lastGoodTop;
        }
      }
      if (e.target.scaleY > this.maxScale) {
        e.target.scaleY = this.maxScale;
        if (e.target.lastGoodLeft && e.target.lastGoodTop) {
          e.target.left = e.target.lastGoodLeft;
          e.target.top = e.target.lastGoodTop;
        }
      }
      e.target.lastGoodTop = e.target.top;
      e.target.lastGoodLeft = e.target.left;
      this.activeObjectPosition.left = e.target.left;
      this.activeObjectPosition.top = e.target.top;
      this.activeObjectPosition.changePosition = true;
      switch (e.target.type) {
        case 'i-text':
          this.textService.activeText.scaleX = parseFloat(e.target.scaleX.toFixed(2));
          this.textService.activeText.scaleY = parseFloat(e.target.scaleY.toFixed(2));
          break;
        case 'image':
          this.imageService.activeImage.scaleX = parseFloat(e.target.scaleX.toFixed(2));
          this.imageService.activeImage.scaleY = parseFloat(e.target.scaleY.toFixed(2));
          break;
        default:
          this.figureService.activeFigure.scaleX = parseFloat(e.target.scaleX.toFixed(2));
          this.figureService.activeFigure.scaleY = parseFloat(e.target.scaleY.toFixed(2));
          break;
      }
    });
  }

  getActiveObject = (e: any) => {
    switch (e.target.type) {
      case 'i-text':
        this.textService.getActiveText(e.target);
        break;
      case 'image':
        this.imageService.addImageTry = false;
        this.imageService.getActiveImage(e.target);
        break;
      default:
        this.figureService.getActiveFigure(e.target);
        break;
    }
  }

  setActiveObjectByIdAndType = (id: number, type: string, canvas: any): void => {
    canvas.setActiveObject(this.getObjectByIdAndType(id, type, canvas));
  }

  getSlideState = () => {
    this.getSlideProperties();
    this.getFigures();
    this.getTexts();
    this.getImages();
  }

  getSlideProperties = () => {
    const canvasProperties = JSON.parse(JSON.stringify(this.canvas.toJSON()));
    this.editedSlide.background.color = canvasProperties.background;
    if (canvasProperties.hasOwnProperty('backgroundImage')) {
      this.editedSlide.background.image = canvasProperties.backgroundImage.src;
    } else {
      this.editedSlide.background.image = '';
    }
  }

  getFigures = () => {
    const canvas = JSON.parse(JSON.stringify(this.canvas.toJSON(['id', 'title'])));
    this.editedSlide.figures.length = 0;
    for (let i = 0; i < canvas.objects.length; ++i) {
      if (['i-text', 'image'].indexOf(canvas.objects[i].type) === -1) {
        const newFigure = {
          id: canvas.objects[i].id,
          title: canvas.objects[i].title,
          fill: canvas.objects[i].fill,
          opacity: canvas.objects[i].opacity,
          left: canvas.objects[i].left,
          top: canvas.objects[i].top,
          scaleX: canvas.objects[i].scaleX,
          scaleY: canvas.objects[i].scaleY,
          angle: canvas.objects[i].angle,
          type: canvas.objects[i].type,
          zIndex: i
        };
        this.editedSlide.figures.push(newFigure);
      }
    }
  }

  getTexts = () => {
    const canvas = JSON.parse(JSON.stringify(this.canvas.toJSON(['id', 'title'])));
    this.editedSlide.texts.length = 0;
    for (let i = 0; i < canvas.objects.length; ++i) {
      if (canvas.objects[i].type === 'i-text') {
        const newText = {
          id: canvas.objects[i].id,
          left: canvas.objects[i].left,
          top: canvas.objects[i].top,
          title: canvas.objects[i].title,
          body: canvas.objects[i].text,
          fontFamily: canvas.objects[i].fontFamily,
          fontSize: canvas.objects[i].fontSize,
          lineHeight: canvas.objects[i].lineHeight,
          textAlign: canvas.objects[i].textAlign,
          fontWeight: canvas.objects[i].fontWeight,
          fontStyle: canvas.objects[i].fontStyle,
          underline: canvas.objects[i].underline,
          linethrough: canvas.objects[i].linethrough,
          fill: canvas.objects[i].fill,
          backgroundColor: canvas.objects[i].backgroundColor,
          opacity: canvas.objects[i].opacity,
          scaleX: canvas.objects[i].scaleX,
          scaleY: canvas.objects[i].scaleY,
          angle: canvas.objects[i].angle,
          type: canvas.objects[i].type,
          zIndex: i
        };
        this.editedSlide.texts.push(newText);
      }
    }
  }

  getImages = () => {
    const canvas = JSON.parse(JSON.stringify(this.canvas.toJSON(['id', 'title'])));
    this.editedSlide.images.length = 0;
    for (let i = 0; i < canvas.objects.length; ++i) {
      if (canvas.objects[i].type === 'image') {
        const newImage = {
          id: canvas.objects[i].id,
          title: canvas.objects[i].title,
          body: canvas.objects[i].src,
          opacity: canvas.objects[i].opacity,
          left: canvas.objects[i].left,
          top: canvas.objects[i].top,
          scaleX: canvas.objects[i].scaleX,
          scaleY: canvas.objects[i].scaleY,
          angle: canvas.objects[i].angle,
          type: canvas.objects[i].type,
          zIndex: i
        };
        this.editedSlide.images.push(newImage);
      }
    }
  }

  setBackgroundImage = (body, canvas) => {
    if (body) {
      fabric.Image.fromURL(body, (img) => {
        canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
          scaleX: canvas.width / img.width,
          scaleY: canvas.height / img.height,
          originX: 'left',
          originY: 'top'
        });
      });
    }
  }

  createPointer = (event: any) => {
    if (!this.pointer.isLocked) {
      let flag = false;
      const pointer = this.canvas.getPointer(event.e);
      this.pointer.x = Math.floor(pointer.x);
      this.pointer.y = Math.floor(pointer.y);
      this.canvas.forEachObject((object) => {
        if (object.pointer) {
          flag = true;
          object.top = this.pointer.y;
          object.left = this.pointer.x;
          this.canvas.requestRenderAll();
          return;
        }
      });
      if (!flag) {
        this.canvas.add(new fabric.Circle({
          zIndex: 10,
          radius: 10,
          top: this.pointer.y,
          left: this.pointer.x,
          fill: 'rgb(255, 0, 0)',
          opacity: 0.3,
          selectable: false,
          pointer: true
        }));
        this.canvas.requestRenderAll();
      }
    }
  }

  deletePointer = () => {
    this.canvas.forEachObject((object) => {
      if (object.pointer) {
        this.canvas.remove(object);
        this.pointer.isLocked = true;
        return;
      }
    });
  }

  changePointerPosition = () => {
    this.canvas.forEachObject((object) => {
      if (object.pointer) {
        object.top = this.pointer.y;
        object.left = this.pointer.x;
        this.canvas.requestRenderAll();
        return;
      }
    });
  }

  bringForward = () => {
    this.canvas.getActiveObject().bringForward();
  }

  sendBackward = () => {
    this.canvas.getActiveObject().sendBackwards();
  }

  getObjectByIdAndType = (id: number, type: string, canvas: any): any => {
    let object: any;
    for (let i = 0; i < canvas.getObjects().length; ++i) {
      if (canvas.item(i).id === id && canvas.item(i).type === type) {
        object = canvas.item(i);
        break;
      }
    }
    return object;
  }

  addExistingObjectsToCanvas = (slide: any, canvas: any, forView: boolean) => {
    if (!canvas) {
      canvas = this.canvas;
    }
    canvas.clear();
    let objects = [];
    for (let i = 0; i < slide.texts.length; ++i) {
      slide.texts[i].type = 'i-text';
      objects.push(slide.texts[i]);
    }
    for (let i = 0; i < slide.figures.length; ++i) {
      objects.push(slide.figures[i]);
    }
    for (let i = 0; i < slide.images.length; ++i) {
      slide.images[i].type = 'image';
      objects.push(slide.images[i]);
    }
    this.changeSlideBackgroundColor(slide.background.color, canvas);
    this.setBackgroundImage(slide.background.image, canvas);
    objects.sort((a, b) => {
      return a.zIndex - b.zIndex;
    });
    if (Object.keys(this.editedSlide).length) {
      slide = JSON.parse(JSON.stringify(this.editedSlide));
    }
    const makeImagesSelectable = !forView;
    for (let i = 0; i < objects.length; ++i) {
      switch (objects[i].type) {
        case 'i-text':
          this.textService.addExistingTextToCanvas(objects[i], canvas, forView);
          break;
        case 'image':
          this.imageService.addExistingImageToCanvas(objects[i], canvas, forView, this.componentRole);
          break;
        default:
          this.figureService.addExistingFigureToCanvas(objects[i], canvas, forView);
          break;
      }
    }
  }

  getActiveObjectAnimations = () => {
    const object = this.canvas.getActiveObject();
    this.animationService.activeObjectAnimations.length = 0;
    for (let i = 0; i < this.editedSlide.animations.length; ++i) {
      if (object.type === this.editedSlide.animations[i].hostType && object.id === this.editedSlide.animations[i].hostId) {
        this.animationService.activeObjectAnimations.push(this.editedSlide.animations[i]);
      }
    }
  }

  changeSlideBackgroundColor = (color, canvas) => {
    canvas.backgroundColor = color;
    canvas.requestRenderAll();
  }

  changeTextBody = (e: any) => {
    this.textService.activeText.body = e.target.text;
  }

  toggleObjectsSelectability = (mode: string, selectStatus: boolean): void => {
    if (this.canvas) {
      this.canvas.forEachObject((object) => {
        object.lockMovementX = false;
        object.lockMovementY = false;
        object.lockScalingX = false;
        object.lockScalingY = false;
        object.lockRotation = false;
      });
      switch (mode) {
        case 'all':
          this.canvas.forEachObject((object) => {
            object.selectable = selectStatus;
            if (object.type === 'i-text') {
              object.editable = selectStatus;
            }
          });
          break;
        case 'figures':
          this.canvas.forEachObject((object) => {
            if (object.type === 'rect' || object.type === 'circle' || object.type === 'triangle') {
              object.selectable = selectStatus;
            }
          });
          break;
        case 'text':
          this.canvas.forEachObject((object) => {
            if (object.type === 'i-text') {
              object.selectable = selectStatus;
              object.editable = true;
            }
          });
          break;
        case 'images':
          this.canvas.forEachObject((object) => {
            if (object.type === 'image') {
              object.selectable = selectStatus;
              object.lockMovementX = true;
              object.lockMovementY = true;
              object.lockScalingX = true;
              object.lockScalingY = true;
              object.lockRotation = true;
            }
          });
          break;
        case 'animation':
          this.canvas.forEachObject((object) => {
            object.selectable = selectStatus;
            object.lockMovementX = true;
            object.lockMovementY = true;
            object.lockScalingX = true;
            object.lockScalingY = true;
            object.lockRotation = true;
          });
          break;
        case 'other':
          this.canvas.forEachObject((object) => {
            object.selectable = false;
            object.lockMovementX = true;
            object.lockMovementY = true;
            object.lockScalingX = true;
            object.lockScalingY = true;
            object.lockRotation = true;
          });
          break;
        default:
          break;
      }
      this.canvas.requestRenderAll();
    }
  }
}