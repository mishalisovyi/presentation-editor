import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw'

const PRES_API: string = "http://localhost:3000/presentations";

@Injectable()
export class PresentationService {

  constructor(private http: Http) { }

  getPresentations = (): Observable<any[]> => {
    return this.http
      .get(PRES_API)
      .map(resp => resp.json())
      .catch((error: any) => Observable.throw(error.json()));
  }

  deletePresentation = (id: number): Observable<any> => {
    return this.http
      .delete(PRES_API + "/" + id)
      .map(success => success.status)
      .catch((error: any) => Observable.throw(error.json()));
  }

  getPresentation = (id: number): Observable<any> => {
    return this.http
      .get(PRES_API + "/" + id)
      .map(resp => resp.json())
      .catch((error: any) => Observable.throw(error.json()));
  }

  editPresentation = (presentation: any): Observable<any> => {
    return this.http
      .put(PRES_API + "/" + presentation.id, presentation)
      .map(success => success.status)
      .catch((error: any) => Observable.throw(error.json()));
  }

  addPresentation = (presentation: any): Observable<any> => {
    return this.http
      .post(PRES_API, presentation)
      .map(success => success.status)
      .catch((error: any) => Observable.throw(error.json()));
  }
}